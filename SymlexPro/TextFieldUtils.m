//
//  TextFieldUtils.m
//  SymlexPro
//
//  Created by USER on 8/13/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "TextFieldUtils.h"
@implementation TextFieldUtils

+(void) setLeftImage:(UITextField *)textField withImageName:(NSString *)imageName{

    
    
}

+(void) addImageRightSideTextView:(UITextField *)textField imageName:(NSString *)image{
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    imageview.image = [UIImage imageNamed:image];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    textField.rightViewMode = UITextFieldViewModeAlways;
    textField.rightView = imageview;
    [textField clipsToBounds];
    
}

+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(void) addImageLeftSideTextView:(UITextField *)textField imageName:(NSString *)image{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 40)];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 24, 24)];
    imageview.image = [UIImage imageNamed:image];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [view addSubview:imageview];
    textField.leftView = view;
    [textField clipsToBounds];
    
}

@end
