//
//  AccountViewController.m
//  SymlexPro
//
//  Created by admin on 9/19/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "AccountViewController.h"
#import "DialpadViewController.h"
#import "Constans.h"
#import "MyApp.h"

@interface AccountViewController ()

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //register an observer for registration callback coming from myapp.m
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processRegState:)
                                                 name: kSIPRegState
                                               object:nil];
    
    [self initStatusButton:@"red_status"];
    
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if([[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
        
        self.contant1.hidden = YES;
        self.contant2.hidden = YES;
        
    }
}

-(void) viewDidAppear:(BOOL)animated{
    
    get_account_status();
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    self.lbAccHolder.text = [userDefault stringForKey:data_user_name];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initStatusButton :(NSString *) imageName{
    
    UIImage *image = [UIImage imageNamed:imageName];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,15, 15)];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.adjustsImageWhenHighlighted = NO;
    UIBarButtonItem *leftButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}


- (void)processRegState:(NSNotification *)notification
{
    NSDictionary *dictionary = [notification userInfo];
    
    //  NSLog(@"Status in phoneview: %d",[[dictionary objectForKey:@"Status"] intValue]);
    if ([[dictionary objectForKey:@"Status"] intValue] == 200){
        [self initStatusButton:@"green_status"];
    }
    else if ([[dictionary objectForKey:@"StatusText"] isEqual:@"registersent"]){
        [self initStatusButton:@"yellow_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 403){
        [self initStatusButton:@"red_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 404){
        [self initStatusButton:@"red_status"];
    }else if ([[dictionary objectForKey:@"Status"] intValue] == 407){
        [self initStatusButton:@"red_status"];
    }
    
    else{
        [self initStatusButton:@"red_status"];
    }
    
}

- (IBAction)dialpadPressed:(id)sender {
    DialpadViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
    [self presentViewController:viewController animated:YES completion:nil];
    
}
@end
