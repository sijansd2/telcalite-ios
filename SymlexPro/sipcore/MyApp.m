#include "MyApp.h"
#include <pjsua-lib/pjsua.h>
#import "Constans.h"
#import "NSNotificationAdditions.h"
#import "AppDelegate.h"
#include "ring.h"


#define THIS_FILE "MyApp.m"

static pjsua_acc_id acc_id=PJSUA_INVALID_ID;
pjsua_acc_config acc_cfg;
pjsua_config cfg;
pj_status_t status;
pjsua_transport_id transport_id_udp = -1;
pjsua_transport_id transport_id_udp6 = -1;
pjsua_transport_id transport_id_tcp = -1;
pjsua_transport_id transport_id_tcp6 = -1;
pjsua_transport_id transport_id_tls = -1;
pjsua_transport_id transport_id_tls6 = -1;
app_config_t    app_config;

static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata);
static void on_call_state(pjsua_call_id call_id, pjsip_event *e);
static void on_call_media_state(pjsua_call_id call_id);
static void on_Reg_Started(pjsua_acc_id acc_id,pj_bool_t boolean);
static void on_Reg_State(pjsua_acc_id acc_id);
static void on_Reg_State2(pjsua_acc_id acc_id,pjsua_reg_info *info);
static void error_exit(const char *title, pj_status_t status);
static void on_transport_state(pjsip_transport *tp,
                               pjsip_transport_state state,
                               const pjsip_transport_state_info *info);
static void sip_manage_codec();
BOOL runningInBackground();
void showNotification(NSString *number);
static void IncomingCalNotification(pjsua_call_id call_id, const pjsua_call_info *ci);
static void CallStateNotification(pjsua_call_id call_id, const pjsua_call_info *ci);
pj_status_t sip_Connect_with_ipv6();
int makeRandomNumber(int minNumber,int maxNumber);

int makeRandomNumber(int minNumber,int maxNumber)
{
    
    NSUInteger i = (arc4random() % (maxNumber - minNumber)) + minNumber;
    return (int)i;
}



int startPjsip()
{
    // Create pjsua first
    acc_id = PJSUA_INVALID_ID;
    
    
    status = pjsua_create();
    if (status != PJ_SUCCESS) error_exit("Error in pjsua_create()", status);
    
    app_config.pool = pjsua_pool_create("application", 1000, 1000);
    // Init pjsua
    {
        // Init the config structure
        
        pjsua_config_default (&cfg);
        cfg.cb.on_incoming_call = &on_incoming_call;
        cfg.cb.on_call_media_state = &on_call_media_state;
        cfg.cb.on_call_state = &on_call_state;
        cfg.cb.on_reg_started=&on_Reg_Started;
        cfg.cb.on_reg_state=&on_Reg_State;
        cfg.cb.on_reg_state2=&on_Reg_State2;
        cfg.cb.on_transport_state=&on_transport_state;
        // Init the logging config structure
        pjsua_logging_config log_cfg;
        pjsua_logging_config_default(&log_cfg);
        log_cfg.console_level = 6;
        
        
        pjsua_media_config_default(&media_cfg);
        
        
        NSUserDefaults *userdef =[NSUserDefaults standardUserDefaults];
        
        NSString *media_ptime = [userdef stringForKey:data_ptime];
        media_cfg.ptime=[media_ptime intValue];
        media_cfg.clock_rate = 8000;
        media_cfg.snd_clock_rate = 8000;
        media_cfg.ec_tail_len = 200;
        media_cfg.no_vad = TRUE;
        media_cfg.snd_auto_close_time = 0;
        
        
        
        if([userdef boolForKey:data_useTurn] && [[userdef stringForKey:data_transport] isEqualToString:@"tls"]){
            media_cfg.enable_ice = TRUE;
        }else{
            media_cfg.enable_ice = FALSE;
        }
        
        if([[userdef objectForKey:@"isIpv6"] isEqualToString:@"true"]){
            cfg.stun_srv[0]= pj_str("192.151.149.100:34564");
            cfg.stun_srv_cnt =1;
            cfg.stun_try_ipv6 = PJ_TRUE;
            cfg.stun_map_use_stun2=PJ_TRUE;
        }else{
            cfg.stun_srv_cnt =0;
            cfg.stun_try_ipv6 = PJ_FALSE;
            cfg.stun_map_use_stun2=PJ_FALSE;
        }
        
        status = pjsua_init(&cfg, &log_cfg, &media_cfg);
        if (status != PJ_SUCCESS) error_exit("Error in pjsua_init()", status);
        
    }
    {
        
        sip_ring_init(&app_config);
    
    }
    
    
    
    // Add UDP transport.
    {
        
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        int val = makeRandomNumber(35000, 50000);
        cfg.port = val;
        // Add UDP transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, &transport_id_udp);
        if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
    }
    
    {
        
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        int val = makeRandomNumber(35000, 50000);
        cfg.port = val;
        // Add UDP transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP6, &cfg, &transport_id_udp6);
        if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
    }
    
    //    // Add TCP transport.
    {
        
        
        // Init transport config structure
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        int val = makeRandomNumber(35000, 50000);
        cfg.port = val;
        
        // Add TCP transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &cfg,&transport_id_tcp);
        if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
    }
    
    {
        
        
        // Init transport config structure
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        int val = makeRandomNumber(35000, 50000);
        cfg.port = val;
        
        // Add TCP transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_TCP6, &cfg,&transport_id_tcp6);
        if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
    }
    
    {
        
        // Init transport config structure
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        int val = makeRandomNumber(35000, 50000);
        cfg.port = val;
        
        // Add TCP transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_TLS, &cfg,&transport_id_tls);
        if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
    }
    
    {
        
        // Init transport config structure
        pjsua_transport_config cfg;
        pjsua_transport_config_default(&cfg);
        int val = makeRandomNumber(35000, 50000);
        cfg.port = val;
        
        // Add TCP transport.
        status = pjsua_transport_create(PJSIP_TRANSPORT_TLS6, &cfg,&transport_id_tls6);
        if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
    }
    
    sip_manage_codec();
    
    
    status = pjsua_start();
    if (status != PJ_SUCCESS) error_exit("Error starting pjsua", status);
    
    
    
    return 1;
}


typedef struct struct_codecs {
    pj_str_t pjsip_name;
} Codecs;

static const Codecs codecs[] = {
    { {"pcmu", 4}},
    {{"pcma", 4}},
    {{"G722/", 5}},
    {{"G7221", 5}},
    {{"GSM", 3}},
    {{"G729", 4}},
    //{@"enableOPENCORE-AMRNB", {"OPENCORE-AMRNB", 14}} // blocked by in version 1.4
};

static void sip_manage_codec()
{
    pj_status_t status;
    
    status = pjsua_codec_set_priority(&codecs[0].pjsip_name,PJMEDIA_CODEC_PRIO_HIGHEST);
    status = pjsua_codec_set_priority(&codecs[1].pjsip_name,PJMEDIA_CODEC_PRIO_HIGHEST);
    status = pjsua_codec_set_priority(&codecs[2].pjsip_name,PJMEDIA_CODEC_PRIO_HIGHEST);
    status = pjsua_codec_set_priority(&codecs[3].pjsip_name,PJMEDIA_CODEC_PRIO_HIGHEST);
    status = pjsua_codec_set_priority(&codecs[4].pjsip_name,PJMEDIA_CODEC_PRIO_HIGHEST);
}
//static void sip_manage_codec()
//{
//
//    const pj_str_t ID_ALL = {"*", 1};
//    pjsua_codec_set_priority(&ID_ALL, PJMEDIA_CODEC_PRIO_DISABLED);
//    pj_str_t codec =pj_str("G729");
//    status = pjsua_codec_set_priority(&codec,
//                                      PJMEDIA_CODEC_PRIO_HIGHEST);
//}



/* */
pj_status_t sip_disconnect()
{
    pj_status_t status = PJ_SUCCESS;
    
    if (pjsua_acc_is_valid(acc_id))
    {
        status = pjsua_acc_del(acc_id);
        if (status == PJ_SUCCESS)
            acc_id = PJSUA_INVALID_ID;
    }
    
    return status;
}

void sip_set_registration(){
    pj_status_t status = PJ_SUCCESS;
    
    if (pjsua_acc_is_valid(acc_id))
    {
        status = pjsua_acc_set_registration(acc_id, PJ_TRUE);
    }
}

void sip_set_unregistration(){
    
    pj_status_t status = PJ_SUCCESS;
    
    if (pjsua_acc_is_valid(acc_id))
    {
        status = pjsua_acc_set_registration(acc_id, PJ_FALSE);
        
    }
    
}


pj_status_t sip_Connect(){
    const char *uname;
    const char *passwd;
    const char *sip_server;
    
    
    NSUserDefaults *theUser = [NSUserDefaults standardUserDefaults];
    NSString *encryption_key = [theUser stringForKey:data_encryption_key];

    char* charKey = (char*)[encryption_key UTF8String];

    int dp_len = [[theUser stringForKey:data_dp_pad] intValue];
    int pd_len = [[theUser stringForKey:data_pd_pad] intValue];

    if([[theUser stringForKey:data_transport] isEqualToString:@"udp"]){
    
        if(![encryption_key isEqualToString:@"0"] && encryption_key.length !=0){
            se_key(1,charKey, (int)encryption_key.length);
            re_key(1,charKey,(int)encryption_key.length, 1,dp_len,pd_len);
        }else{
            se_key(0,charKey, (int)encryption_key.length);
            re_key(0,charKey,(int)encryption_key.length, 0,dp_len,pd_len);
        }
    }else{
        se_key(0,charKey, (int)encryption_key.length);
        re_key(0,charKey,(int)encryption_key.length, 0,dp_len,pd_len);
        
    }
    
    uname  = [[theUser stringForKey:data_user_name] UTF8String];
    passwd = [[theUser stringForKey:data_password] UTF8String];
    sip_server = [[theUser stringForKey:data_sip_ip] UTF8String];
    
    NSString *proxy_server = [theUser stringForKey:data_proxy_ip];
    
    if(proxy_server.length==0){
        proxy_server=[theUser stringForKey:data_sip_ip];
    }
    else{
        //added by sijan
        if([theUser boolForKey:data_useTurn] && [[theUser stringForKey:data_transport] isEqual:@"tls"]){
            proxy_server = [theUser stringForKey:data_proxyPort];
        }
    }
    
    pjsua_acc_config_default(&acc_cfg);
    
    //reg id
    char sipId[PJSIP_MAX_URL_SIZE];
    sprintf(sipId, "\"%s\"<sip:%s@%s>",uname, uname, sip_server);
    acc_cfg.id = pj_str(sipId);
    
    if ((status = pjsua_verify_sip_url(acc_cfg.id.ptr)) != 0)
    {
        PJ_LOG(1,(THIS_FILE, "Error: invalid SIP URL '%s' in local id argument",
                  acc_cfg.id));
        // [app displayParameterError: @"Invalid value for username or server."];
        return status;
    }
    
    
    //reg uri
    char regUri[PJSIP_MAX_URL_SIZE];
    sprintf(regUri, "sip:%s", sip_server);
    acc_cfg.reg_uri = pj_str(regUri);
    
    if ((status = pjsua_verify_sip_url(acc_cfg.reg_uri.ptr)) != 0)
    {
        PJ_LOG(1,(THIS_FILE,  "Error: invalid SIP URL '%s' in registrar argument",
                  acc_cfg.reg_uri));
        //  [app displayParameterError: @"Invalid value for server parameter."];
        return status;
    }
    
    //reg credential
    acc_cfg.cred_count=1;
    acc_cfg.cred_info[0].username = pj_str((char*)uname);
    acc_cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    acc_cfg.cred_info[0].data = pj_str((char *)passwd);
    acc_cfg.cred_info[0].realm = pj_str("*");
    acc_cfg.cred_info[0].scheme=pj_str((char*)"Digest");
    acc_cfg.reg_timeout=600;
    acc_cfg.ka_interval=600;
    acc_cfg.reg_retry_interval=15;
    acc_cfg.proxy_cnt=1;
    acc_cfg.media_stun_use = PJSUA_STUN_USE_DISABLED;
    acc_cfg.sip_stun_use = PJSUA_STUN_USE_DISABLED;
    
    //added by sijan
    if([theUser boolForKey:data_useTurn] && [[theUser stringForKey:data_transport] isEqual:@"tls"]){
        const char *turn_server = [[theUser stringForKey:data_turn_ip] UTF8String];
        acc_cfg.ice_cfg.enable_ice = TRUE;
        acc_cfg.ice_cfg.ice_max_host_cands = 1;
        acc_cfg.turn_cfg.turn_server = pj_str((char *)turn_server);
        acc_cfg.turn_cfg.turn_conn_type = PJ_TURN_TP_TCP;
        acc_cfg.turn_cfg.enable_turn = TRUE;
    }else{
        acc_cfg.ice_cfg.enable_ice = 0;
        acc_cfg.ice_cfg_use=0;
        acc_cfg.turn_cfg_use=0;
        acc_cfg.ice_cfg.enable_ice=0;
        acc_cfg.turn_cfg.enable_turn=0;
    }
    
    if(proxy_server && proxy_server.length >0){
        NSString *temp;
        if([[theUser stringForKey:data_transport] isEqual:@"udp"])
        {
            acc_cfg.srtp_secure_signaling = 0;
            acc_cfg.use_srtp = PJMEDIA_SRTP_DISABLED;
            temp=[NSString stringWithFormat:@"sip:%@;lr;transport=udp",proxy_server];
            
        }else if([[theUser stringForKey:data_transport] isEqual:@"tcp"])
        {
            acc_cfg.srtp_secure_signaling = 0;
            acc_cfg.use_srtp = PJMEDIA_SRTP_DISABLED;
            temp=[NSString stringWithFormat:@"sip:%@;lr;transport=tcp",proxy_server];
            
        }else if([[theUser stringForKey:data_transport] isEqual:@"tls"])
        {
         temp=[NSString stringWithFormat:@"sip:%@;lr;transport=tls",proxy_server];
            //added by sijan
            if([theUser boolForKey:data_useTurn]){
                acc_cfg.srtp_secure_signaling = 1;
                acc_cfg.use_srtp = PJMEDIA_SRTP_DISABLED;
            }else{
                acc_cfg.srtp_secure_signaling = 1;
                acc_cfg.use_srtp = PJMEDIA_SRTP_MANDATORY;
            }
        }
        
        char *proxi = (char *)[temp UTF8String];
        
        if ((status = pjsua_verify_sip_url(proxi)) != 0)
        {
            PJ_LOG(1,(THIS_FILE,  "Error: invalid SIP URL in proxy argument (%d)",
                      proxi, status));
            //[app displayParameterError: @"Invalid value for proxy parameter."];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Invalid value for proxy parameter."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }else{
            
            if (acc_cfg.proxy_cnt==PJ_ARRAY_SIZE(acc_cfg.proxy))
            {
                PJ_LOG(1,(THIS_FILE, "Error: too many proxy servers"));
                
            }else{
                acc_cfg.proxy[0]=pj_str(proxi);
            }
        }
    }
    
    
    if(acc_id != PJSUA_INVALID_ID)
    {
        status = pjsua_acc_del(acc_id);
        if (status == PJ_SUCCESS)
            acc_id = PJSUA_INVALID_ID;
        
    }
    
    @try {
        status = pjsua_acc_add(&acc_cfg, PJ_TRUE, &acc_id);
        if (status != PJ_SUCCESS){
            error_exit("Error adding account", status);
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    return status;
    
}

static void on_transport_state(pjsip_transport *tp,
                               pjsip_transport_state state,
                               const pjsip_transport_state_info *info){

    char host_port[128];

   // NSLog(@"on_transport_state---------------1 %s",tp->info);
    
    pj_ansi_snprintf(host_port, sizeof(host_port), "[%.*s:%d]",
                     (int)tp->remote_name.host.slen,
                     tp->remote_name.host.ptr,
                     tp->remote_name.port);

}
/* Callback called by the library upon receiving incoming call */
static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id,
                             pjsip_rx_data *rdata)
{
    pjsua_call_info ci;
    
    PJ_UNUSED_ARG(acc_id);
    PJ_UNUSED_ARG(rdata);
    
    pjsua_call_get_info(call_id, &ci);
    
    NSLog(@"MYAPP----INCOMING CALL______");
    
    PJ_LOG(3,(THIS_FILE, "Incoming call from %.*s!!",
              (int)ci.remote_info.slen,
              ci.remote_info.ptr));
    
    /* Automatically answer incoming calls with 200/OK */
    pjsua_call_answer(call_id, 180, NULL, NULL);
    IncomingCalNotification(call_id,&ci);
    
    //added by riaz for notification incoming call
    NSString *remoteInfo = @"";
    remoteInfo = [NSString stringWithUTF8String:ci.remote_info.ptr];
    
    NSArray *arr =[remoteInfo componentsSeparatedByString:@":"];
    NSArray *add = [arr[1] componentsSeparatedByString:@"@"];
    if (runningInBackground()) {
        
        showNotification(add[0]);
        
    }
    
    sip_ring_start(&app_config);
}

/* Callback called by the library when call's state has changed */
static void on_call_state(pjsua_call_id call_id, pjsip_event *e)
{
    pjsua_call_info ci;
    
    PJ_UNUSED_ARG(e);
    
    pjsua_call_get_info(call_id, &ci);
    
    
    if(ci.state == PJSIP_INV_STATE_DISCONNECTED){
        
        [UIApplication sharedApplication].idleTimerDisabled=NO;
        
        sip_ring_stop(&app_config);
        //pjsua_conf_disconnect(ci.conf_slot, 0);
        //pjsua_conf_disconnect(0, ci.conf_slot);
    }
    
    CallStateNotification(call_id,&ci);
    PJ_LOG(3,(THIS_FILE, "Call %d state=%.*s", call_id,
              (int)ci.state_text.slen,
              ci.state_text.ptr));
}

/* Callback called by the library when call's media state has changed */
static void on_call_media_state(pjsua_call_id call_id)
{
    pjsua_call_info ci;
    
    pjsua_call_get_info(call_id, &ci);
    sip_ring_stop(&app_config);
    
    
    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE || ci.media_status == PJSUA_CALL_MEDIA_REMOTE_HOLD) {
        // When media is active, connect call to sound device.
        pjsua_conf_connect(ci.conf_slot, 0);
        pjsua_conf_connect(0, ci.conf_slot);
    }
}



/* Callback called by the library when registration started */
static void on_Reg_Started(pjsua_acc_id acc_id,pj_bool_t boolean)
{
    NSLog(@"on_Reg_Started---------------");
    
    pj_status_t status;
    pjsua_acc_info info;
    
    NSString *accUri = @"",*statusText = @"";
    
    status = pjsua_acc_get_info(acc_id, &info);
    if (status != PJ_SUCCESS){
        
        error_exit("Error in pjsua_init()", status);
        return;
    }
    
    
    if (info.acc_uri.slen)
        accUri = [NSString stringWithUTF8String:info.acc_uri.ptr];
    
    if (info.status_text.slen)
        statusText = [NSString stringWithUTF8String:info.status_text.ptr];
    
    NSLog(@"Status---%u----code---%@",info.reg_last_err,statusText);
    
}

/* Callback called by the library when registration has changed */
static void on_Reg_State(pjsua_acc_id acc_id)
{
    
    NSLog(@"on_Reg_State---------------");

    
    pj_status_t status;
    pjsua_acc_info info;
    NSString *accUri = @"", *statusText = @"";
    
    
    status = pjsua_acc_get_info(acc_id, &info);
    if (status != PJ_SUCCESS){
        
        error_exit("Error in pjsua_init()", status);
        return;
    }
    
    if (info.acc_uri.slen)
        accUri = [NSString stringWithUTF8String:info.acc_uri.ptr];
    
    if (info.status_text.slen)
        statusText = [NSString stringWithUTF8String:info.status_text.ptr];
    
    //NSLog(@"Status---%u----code---%@",info.reg_last_err,statusText);
    
    NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithInt: acc_id], @"AccountID",
                              [NSNumber numberWithBool:info.is_default], @"IsDefault",
                              accUri, @"AccountUri",
                              [NSNumber numberWithBool:info.has_registration], @"HasRegistration",
                              [NSNumber numberWithInt: info.expires], @"Expires",
                              [NSNumber numberWithInt: info.status], @"Status",
                              statusText, @"StatusText",
                              [NSNumber numberWithBool:info.online_status], @"OnlineStatus",
                              nil];
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSIPRegState
                                                                        object:nil
                                                                      userInfo:userinfo];
    
}

/* Callback called by the library when registration has changed */
static void on_Reg_State2(pjsua_acc_id acc_id,pjsua_reg_info *info)
{
    
    pj_status_t status;
    pjsua_acc_info infos;
    
    status = pjsua_acc_get_info(acc_id, &infos);
    if (status != PJ_SUCCESS){
        
        error_exit("Error in pjsua_init()", status);
        return;
    }
    
    NSString *accUri = @"", *statusText = @"";
    
    if (infos.acc_uri.slen)
        accUri = [NSString stringWithUTF8String:infos.acc_uri.ptr];
    
    NSLog(@"account uri---%s",infos.acc_uri.ptr);
    
    
    
    if (infos.status_text.slen)
        statusText = [NSString stringWithUTF8String:infos.status_text.ptr];
    
    //Condition added for ipv6
    
    NSLog(@"Status---%u----code---%@",infos.status,statusText);
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    
    if([[userdef objectForKey:@"isIpv6"] isEqualToString:@"true"] && [statusText isEqualToString:@"Service Unavailable"]){
    
        sip_Connect_with_ipv6();
        
    }
    
    NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithInt: acc_id], @"AccountID",
                              [NSNumber numberWithBool:infos.is_default], @"IsDefault",
                              accUri, @"AccountUri",
                              [NSNumber numberWithBool:infos.has_registration], @"HasRegistration",
                              [NSNumber numberWithInt: infos.expires], @"Expires",
                              [NSNumber numberWithInt: infos.status], @"Status",
                              statusText, @"StatusText",
                              [NSNumber numberWithBool:infos.online_status], @"OnlineStatus",
                              //pj_str_t		online_status_text;
                              //pjrpid_element	rpid;
                              nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSIPRegState
                                                                        object:nil
                                                                      userInfo:userinfo];
    
    NSDictionary *userin = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"aaa",@"ssss",
                              nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kBalanceState
                                                                        object:nil
                                                                      userInfo:userin];
}
 void restartPJSIP(){

    pjsua_destroy();
    startPjsip();
    sip_Connect();
}

/* Display error and exit pjsua */
static void error_exit(const char *title, pj_status_t status)
{
    pjsua_perror(THIS_FILE, title, status);
    
    /* Destroy pjsua */
    status = pjsua_destroy();
    
    //exit(1);
    
}


static void IncomingCalNotification(pjsua_call_id call_id, const pjsua_call_info *ci)
{
    NSString *remoteInfo = @"", *remoteContact = @"",*call_id_string=@"";
    
    if (ci->remote_info.slen)
        remoteInfo = [NSString stringWithUTF8String:ci->remote_info.ptr];
    if (ci->remote_contact.slen)
        remoteContact = [NSString stringWithUTF8String:ci->remote_contact.ptr];
    
    if (ci->call_id.slen)
        call_id_string = [NSString stringWithUTF8String:ci->call_id.ptr];
    
    
    //NSLog(@"Call ID---%d",call_id);
    NSString *StatusText = [NSString stringWithFormat:@"%s", ci->last_status_text.ptr ];
    // FIXME: create an Object, InCall for example ?
    NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithInt: call_id], @"CallID",
                              [NSNumber numberWithInt: ci->role], @"Role",
                              [NSNumber numberWithInt: ci->acc_id], @"AccountID",
                              remoteInfo, @"RemoteInfo",
                              remoteContact, @"RemoteContact",
                              StatusText,@"StatusText",
                              [NSNumber numberWithInt: ci->state], @"State",
                              [NSNumber numberWithInt:ci->last_status], @"LastStatus",
                              [NSNumber numberWithInt:ci->media_status], @"MediaStatus",
                              call_id_string, @"callIdStr",
                              [NSNumber  numberWithLong:ci->connect_duration.sec], @"ConnectDuration",
                              [NSNumber  numberWithLong:ci->total_duration.sec], @"TotalDuration",
                              nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSIPINCOMINGCall
                                                                        object:nil
                                                                      userInfo:userinfo];
    
}


static void CallStateNotification(pjsua_call_id call_id, const pjsua_call_info *ci)
{
    NSString *remoteInfo = @"", *remoteContact = @"",*call_id_string=@"";
    
    if (ci->remote_info.slen)
        remoteInfo = [NSString stringWithUTF8String:ci->remote_info.ptr];
    if (ci->remote_contact.slen)
        remoteContact = [NSString stringWithUTF8String:ci->remote_contact.ptr];
    
    if (ci->call_id.slen)
        call_id_string = [NSString stringWithUTF8String:ci->call_id.ptr];
    
    
    
    //NSLog(@"Call ID---%d",call_id);
    NSString *StatusText = [NSString stringWithFormat:@"%s", ci->last_status_text.ptr ];
    // FIXME: create an Object, InCall for example ?
    NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithInt: call_id], @"CallID",
                              [NSNumber numberWithInt: ci->role], @"Role",
                              [NSNumber numberWithInt: ci->acc_id], @"AccountID",
                              remoteInfo, @"RemoteInfo",
                              remoteContact, @"RemoteContact",
                              StatusText,@"StatusText",
                              [NSNumber numberWithInt: ci->state], @"State",
                              [NSNumber numberWithInt:ci->last_status], @"LastStatus",
                              [NSNumber numberWithInt:ci->media_status], @"MediaStatus",
                              call_id_string, @"callIdStr",
                              [NSNumber  numberWithLong:ci->connect_duration.sec], @"ConnectDuration",
                              [NSNumber  numberWithLong:ci->total_duration.sec], @"TotalDuration",
                              nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSIPCallState
                                                                        object:nil
                                                                      userInfo:userinfo];
}

pj_status_t makeCall(char* destUri,pjsua_call_id *call_id,pjsua_call_setting call_opt)
{
    pj_status_t status;
    pj_str_t uri = pj_str(destUri);
    
    status = pjsua_verify_sip_url(destUri);
    
    if(status==PJSIP_EINVALIDURI){
        NSLog(@"Fault in URI");
        
    }
    
    if(status !=PJ_SUCCESS){
        
        NSLog(@"Invalid URI");
        return status;
        
    }
    
    
    
    
    status = pjsua_call_make_call(acc_id, &uri, &call_opt, NULL, NULL, call_id);
    //if (status != PJ_SUCCESS) error_exit("Error making call", status);
    
    
    return status;
}

void endCall()
{
    
    pjsua_call_hangup_all();
}

void unholdCall(pjsua_call_id call_id,pjsua_call_setting call_opt){

    call_opt.flag |= PJSUA_CALL_UNHOLD;
    pjsua_call_reinvite2(call_id, &call_opt, NULL);
}


void acceptcall(pjsua_call_id *callID){
    
    status = pjsua_call_answer(*callID, 200, NULL, NULL);
    
    
}

//added by riaz for checking is backgroundstate
BOOL runningInBackground()
{
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    BOOL result = (state == UIApplicationStateBackground);
    
    return result;
}

void showNotification(NSString *number)
{
    // Create a new notification
    UILocalNotification* alert = [[UILocalNotification alloc] init];
    if (alert)
    {
        NSString *dar =@"Incoming call from ";
        dar = [dar stringByAppendingString:number];
        alert.repeatInterval = 30;
        alert.alertBody = dar;
        /* This action just brings the app to the FG, it doesn't
         * automatically answer the call (unless you specify the
         * --auto-answer option).
         */
        alert.alertAction = @"Activate app";
        
        [[UIApplication sharedApplication] presentLocalNotificationNow:alert];
    }
    
}

void get_account_status(){

    NSString *accUri = @"", *statusText = @"";
    
    if(acc_id != PJSUA_INVALID_ID){
    
        pjsua_acc_info infos;
        
        status = pjsua_acc_get_info(acc_id, &infos);
        if (status != PJ_SUCCESS){
            
            return;
        }
        
        if (infos.acc_uri.slen)
            accUri = [NSString stringWithUTF8String:infos.acc_uri.ptr];
        
        if (infos.status_text.slen)
            statusText = [NSString stringWithUTF8String:infos.status_text.ptr];
        
        NSLog(@"Status---%u----code---%@",infos.reg_last_err,statusText);
        
        NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithInt: acc_id], @"AccountID",
                                  [NSNumber numberWithBool:infos.is_default], @"IsDefault",
                                  accUri, @"AccountUri",
                                  [NSNumber numberWithBool:infos.has_registration], @"HasRegistration",
                                  [NSNumber numberWithInt: infos.expires], @"Expires",
                                  [NSNumber numberWithInt: infos.status], @"Status",
                                  statusText, @"StatusText",
                                  [NSNumber numberWithBool:infos.online_status], @"OnlineStatus",
                                  nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSIPRegState
                                                                            object:nil
                                                                          userInfo:userinfo];
    }
}


void sip_call_play_digit(pjsua_call_id call_id, char digit)
{
    pj_status_t status;
    char buf[2];
    pj_str_t digits;
    
    buf[0] = digit;
    buf[1] = 0;
    digits = pj_str(buf);
    
    status = pjsua_call_dial_dtmf(call_id, &digits);
    if (status == PJMEDIA_RTP_EREMNORFC2833)
    {
        pjmedia_tone_digit d[1];
        struct my_call_data *cd;
        
        cd = (struct my_call_data*) pjsua_call_get_user_data(call_id);
        d[0].digit = digit;
        d[0].on_msec = 100;
        d[0].off_msec = 100;
        d[0].volume = 16383;
        
    }
}


pj_status_t sip_Connect_with_ipv6(){
    const char *uname;
    const char *passwd;
    const char *sip_server;
    NSString *media_ptime;
    
    
    NSUserDefaults *theUser = [NSUserDefaults standardUserDefaults];
    NSString *encryption_key = [theUser stringForKey:data_encryption_key];
    
    char* charKey = (char*)[encryption_key UTF8String];
    
    int dp_len = [[theUser stringForKey:data_dp_pad] intValue];
    int pd_len = [[theUser stringForKey:data_pd_pad] intValue];
    
    if(![encryption_key isEqualToString:@"0"] && encryption_key.length !=0){
        
        se_key(1,charKey, (int)encryption_key.length);
        re_key(1,charKey,(int)encryption_key.length, 1,dp_len,pd_len);
    }else{
        
        se_key(0,charKey, (int)encryption_key.length);
        re_key(0,charKey,(int)encryption_key.length, 0,dp_len,pd_len);
    }
    
    uname  = [[theUser stringForKey:data_user_name] UTF8String];
    passwd = [[theUser stringForKey:data_password] UTF8String];
    sip_server = [[theUser stringForKey:data_sip_ip] UTF8String];
    
    NSString *proxy_server = [theUser stringForKey:data_proxy_ip];
    
    if(proxy_server.length==0){
        
        proxy_server=[theUser stringForKey:data_sip_ip];
    }
    
    pjsua_acc_config_default(&acc_cfg);
    
    //reg id
    char sipId[PJSIP_MAX_URL_SIZE];
    sprintf(sipId, "\"%s\"<sip:%s@%s>",uname, uname, sip_server);
    acc_cfg.id = pj_str(sipId);
    
    if ((status = pjsua_verify_sip_url(acc_cfg.id.ptr)) != 0)
    {
        PJ_LOG(1,(THIS_FILE, "Error: invalid SIP URL '%s' in local id argument",
                  acc_cfg.id));
        // [app displayParameterError: @"Invalid value for username or server."];
        return status;
    }
    
    
    //reg uri
    char regUri[PJSIP_MAX_URL_SIZE];
    sprintf(regUri, "sip:%s", sip_server);
    acc_cfg.reg_uri = pj_str(regUri);
    
    if ((status = pjsua_verify_sip_url(acc_cfg.reg_uri.ptr)) != 0)
    {
        PJ_LOG(1,(THIS_FILE,  "Error: invalid SIP URL '%s' in registrar argument",
                  acc_cfg.reg_uri));
        //  [app displayParameterError: @"Invalid value for server parameter."];
        return status;
    }
    
    //reg credential
    acc_cfg.cred_count=1;
    acc_cfg.cred_info[0].username = pj_str((char*)uname);
    acc_cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    acc_cfg.cred_info[0].data = pj_str((char *)passwd);
    acc_cfg.cred_info[0].realm = pj_str("*");
    acc_cfg.cred_info[0].scheme=pj_str((char*)"Digest");
    acc_cfg.reg_timeout=600;
    acc_cfg.ka_interval=600;
    acc_cfg.reg_retry_interval=15;
    acc_cfg.proxy_cnt=1;
    acc_cfg.ice_cfg_use=0;
    acc_cfg.turn_cfg_use=0;
    acc_cfg.ice_cfg.enable_ice=0;
    acc_cfg.turn_cfg.enable_turn=0;
    acc_cfg.transport_id = transport_id_udp6;
    acc_cfg.ipv6_media_use = PJSUA_IPV6_ENABLED;
    acc_cfg.nat64_opt = PJSUA_NAT64_ENABLED;
    
    
    
    if(proxy_server && proxy_server.length >0){
        NSString *temp;
        if([[theUser stringForKey:data_transport] isEqual:@"udp"])
        {
            acc_cfg.srtp_secure_signaling = 0;
            acc_cfg.use_srtp = PJMEDIA_SRTP_DISABLED;
            temp=[NSString stringWithFormat:@"sip:%@;lr;transport=udp",proxy_server];
            
        }else if([[theUser stringForKey:data_transport] isEqual:@"tcp"])
        {
            acc_cfg.srtp_secure_signaling = 0;
            acc_cfg.use_srtp = PJMEDIA_SRTP_DISABLED;
            temp=[NSString stringWithFormat:@"sip:%@;lr;transport=tcp",proxy_server];
            
        }else if([[theUser stringForKey:data_transport] isEqual:@"tls"])
        {
            
            temp=[NSString stringWithFormat:@"sip:%@;lr;transport=tls",proxy_server];
            acc_cfg.srtp_secure_signaling = 1;
            acc_cfg.use_srtp = PJMEDIA_SRTP_MANDATORY;
            
        }
        
        char *proxi = (char *)[temp UTF8String];
        
        if ((status = pjsua_verify_sip_url(proxi)) != 0)
        {
            PJ_LOG(1,(THIS_FILE,  "Error: invalid SIP URL in proxy argument (%d)",
                      proxi, status));
            //[app displayParameterError: @"Invalid value for proxy parameter."];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Invalid value for proxy parameter."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }else{
            
            if (acc_cfg.proxy_cnt==PJ_ARRAY_SIZE(acc_cfg.proxy))
            {
                PJ_LOG(1,(THIS_FILE, "Error: too many proxy servers"));
                
            }else{
                acc_cfg.proxy[0]=pj_str(proxi);
            }
        }
    }
    
    
    if(acc_id != PJSUA_INVALID_ID)
    {
        status = pjsua_acc_del(acc_id);
        if (status == PJ_SUCCESS)
            acc_id = PJSUA_INVALID_ID;
        
    }
    
    @try {
        status = pjsua_acc_add(&acc_cfg, PJ_TRUE, &acc_id);
        if (status != PJ_SUCCESS){
            error_exit("Error adding account", status);
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    return status;
    
}
