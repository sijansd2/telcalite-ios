//
//  DialpadViewController.m
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "ReviewDialpadViewController.h"
#import "Constans.h"
#import <AudioToolbox/AudioToolbox.h>
#import "UIView+Toast.h"
#import "AFNetworking.h"
#import "CallViewController.h"
#import "MyApp.h"
#import "NSNotificationAdditions.h"
#import "AppDelegate.h"
#import "Base64.h"
#import "LoginTabViewController.h"

@interface ReviewDialpadViewController (){
    NSUserDefaults* userDefault;
}

@end
Dialer_keypadView *dialerKeyPad;

@implementation ReviewDialpadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configPushsService];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    
    //[self setNeedsStatusBarAppearanceUpdate];
    
    //focus the dialpad text so the cursor is always visible
    self.tfDialpad.delegate=self;
    
    //for hiding keyboard we use a dummyview view as input view of dialpad
    UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.tfDialpad.inputView = dummyView;
    [self.tfDialpad becomeFirstResponder];
    self.lbRegStatus.text=@"Trying To Register";

    //assign long press in delete button
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(DeletelongPress:)];
    [self.btnDelete addGestureRecognizer:longPress];
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(netStat:)
                                                 name: NetworkUpdate
                                               object:nil];
    
    //register an observer for registration callback coming from myapp.m
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processRegState:)
                                                 name: kSIPRegState
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(balanceUpdate)
                                                 name: kBalanceState
                                               object:nil];
    
    //[self getAddressbookPermission];
    
}

-(void) viewWillAppear:(BOOL)animated{

    if(dialingNumber){
        self.tfDialpad.text = dialingNumber;
        dialingNumber = @"";
    }
    [self.tfDialpad becomeFirstResponder];
    
    //self.lbRegStatus.text = @"Trying to register..";
    //[self balanceUpdate];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) keypadPressedWithValue:(NSString *)value{
    
    //get cursor position
    UITextRange *cursorPosition = self.tfDialpad.selectedTextRange;
    //get the cursor exact location in int
    NSInteger endOffset = [self.tfDialpad offsetFromPosition:self.tfDialpad.beginningOfDocument toPosition:cursorPosition.end];
    
    
    //comp dialpadtext
    NSMutableString * temp = [[NSMutableString alloc] initWithString:self.tfDialpad.text];
    
    //insert the new data in current cursor position
    [temp insertString:value atIndex:endOffset];
    //temp = [temp stringByAppendingString:value];
    
    //update dialpadText with new data
    self.tfDialpad.text=temp;
    
    //set cursor postion for next data
    UITextPosition *nextposition = [self.tfDialpad positionFromPosition:cursorPosition.end offset:1];
    
    [self.tfDialpad setSelectedTextRange:[self.tfDialpad textRangeFromPosition:nextposition
                                                              toPosition:nextposition]];
    
    
    
    //[self changeCallBtnColler];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
-(void) DeletelongPress:(UILongPressGestureRecognizer*)gesture{
    
    //emty dialpadText text in long press
    self.tfDialpad.text=@"";
    
}


- (IBAction)deletePresses:(id)sender {
    
    //get current cursor position
    UITextRange *cursorPosition = self.tfDialpad.selectedTextRange;
    
    NSInteger endOffset = [self.tfDialpad offsetFromPosition:self.tfDialpad.beginningOfDocument toPosition:cursorPosition.end];
    
    
    NSString *str = self.tfDialpad.text;
    if(str.length !=0 && endOffset>0){
        //remove the current charcter in current cursor position
        str = [str stringByReplacingCharactersInRange:NSMakeRange(endOffset-1, 1) withString:@""];
        
        //NSString *truncatedString = [str substringToIndex:[str length]-1];
        
        //update dialpadText
        self.tfDialpad.text = str;
        
        //update cursor position
        UITextPosition *nextposition = [self.tfDialpad positionFromPosition:cursorPosition.end offset:-1];
        
        [self.tfDialpad setSelectedTextRange:[self.tfDialpad textRangeFromPosition:nextposition
                                                                  toPosition:nextposition]];
        
        
    }
}

-(IBAction) btnCancelPressed:(UIButton *)sender{

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction) keyEvent:(UIButton *)sender{

    if(sender.tag ==10){
        [self sendToPhoneView:@"*"];
    }else if(sender.tag == 11){
        [self sendToPhoneView:@"#"];
    }else{
        [self sendToPhoneView:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    }
    
}

-(void) sendToPhoneView:(NSString*)number{
    
    //play sound for the pressed key
    [self playSoundForKey:number];
    
    [self keypadPressedWithValue:number];
 
}
- (void)playSoundForKey:(NSString*)key
{
    
    //get the pound key tone
    if([key  isEqual:@"*"]){
        
        key=@"s";
        
    }
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filename = [NSString stringWithFormat:@"dtmf-%@",key];
    
    //get file path
    NSString *path = [mainBundle pathForResource:filename ofType:@"aif"];
    if (!path)
        return;
    //convert it to the url
    NSURL *aFileURL = [NSURL fileURLWithPath:path isDirectory:NO];
    if (aFileURL != nil)
    {
        SystemSoundID aSoundID;
        OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)aFileURL,
                                                          &aSoundID);
        if (error != kAudioServicesNoError)
            return;
        //play using avaudioplayer
        AudioServicesPlaySystemSound(aSoundID);
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)processRegState:(NSNotification *)notification
{
    NSDictionary *dictionary = [notification userInfo];
    
    //  NSLog(@"Status in phoneview: %d",[[dictionary objectForKey:@"Status"] intValue]);
    if ([[dictionary objectForKey:@"Status"] intValue] == 200){
        self.lbRegStatus.text = @"Ready to call";
    }
    else if ([[dictionary objectForKey:@"StatusText"] isEqual:@"registersent"]){
        self.lbRegStatus.text = @"Trying to register..";
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 403){
        self.lbRegStatus.text = @"User not found";
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 404){
        self.lbRegStatus.text = @"Forbidden";
    }else if ([[dictionary objectForKey:@"Status"] intValue] == 407){
        restartPJSIP();
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 408){
        self.lbRegStatus.text = @"Request time out";
    }
    
    else{
        self.lbRegStatus.text = [dictionary objectForKey:@"StatusText"];
    }
    
    self.lbHeader.text = [userDefault stringForKey:data_header];
    self.lbFooter.text = [userDefault stringForKey:data_footer];
    
}


-(void)balanceUpdate{
    [self.tfDialpad becomeFirstResponder];
    if([[userDefault stringForKey:data_kaInterval] isEqualToString:@"15"]){
        [self getBalanceFor15];
    }
    else{
        [self getDefaultBalance];
    }
}



-(void) getFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    
    NSLog(@"%@",responseObject);
    if(tag == 100){
        
        if (![responseObject isEqualToString:@"99"])
        {
           NSLog(@"%@",responseObject[@"message"]);
        }
    }
    
}

-(void) getFinishedWithError:(NSError *)error{
    
    NSLog(@"%@",error);
}


-(void)netStat:(NSNotification *)notification{
    
    
    NSLog(@"%@",[ notification userInfo ]);
    
    int state = [[[ notification userInfo ] objectForKey: @"netStat"] intValue];
    
    if(state==1){
        [self balanceUpdate];
    
    }
    
}



- (IBAction)makeCallPressed:(id)sender {
    
    NSString *lastNum = [userDefault stringForKey:data_dialed_number];
    if(lastNum != nil && ![lastNum isEqualToString:@""] && [self.tfDialpad.text isEqualToString:@""]){
        self.tfDialpad.text = lastNum;
    }
    else{
    
    NSString *number = self.tfDialpad.text;
    pjsua_acc_id acc_id = 0;
    pjsua_acc_info infos;
    
    pjsua_acc_get_info(acc_id,&infos);
    //check if number is available in dialpadtext
    if(number.length >0){
        //check current registration status
        if(infos.status==200 && infos.expires>0){
            
            [userDefault removeObjectForKey:data_dialed_number];
            [userDefault setObject:number forKey:data_dialed_number];
            
            CallViewController *callView = [self.storyboard instantiateViewControllerWithIdentifier:@"callviewcontrollerId"];
            callView.phoneNumber=number;
            callView.callType=callTypeDialed;
            [self presentViewController:callView animated:YES completion:nil];
            
        }
    }else{
        
        NSLog(@"No number pressed");
        
    }
    }
}

- (IBAction)addPressed:(id)sender {
    
//    CallViewController *callView = [self.storyboard instantiateViewControllerWithIdentifier:@"callviewcontrollerId"];
//    callView.phoneNumber=[userDefault stringForKey:data_ivr];
//    callView.callType=callTypeDialed;
//    [self presentViewController:callView animated:YES completion:nil];
}


-(void)getDefaultBalance{
    
    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        NSString* user = [userDefault stringForKey:data_user_name];
        NSString* url = [NSString stringWithFormat: @"%@%@%@/%@",baseUrl,balance,user,apiKey];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@",responseObject);
            NSDictionary *jsonOutput = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            
            if([jsonOutput[@"message"] isEqualToString:@"success"]){
                self.lblBalance.text = [NSString stringWithFormat:@"Balance: %@",jsonOutput[@"balance"]];
                NSLog(@"%@",jsonOutput[@"balance"]);
            }
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"%@",error);
            self.lblBalance.text = @"Balance: N/A";
            
        }];
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
}


-(void)getBalanceFor15{
    
    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        NSString* user = [userDefault stringForKey:data_user_name];
        NSString* pass = [userDefault stringForKey:data_password];
        NSString* url = [[userDefault stringForKey:data_balancelink] stringByReplacingOccurrencesOfString:@"##user##" withString:user];
        url = [url stringByReplacingOccurrencesOfString:@"##pass##" withString:pass];
        
        dispatch_async(kBgQueues, ^{
            NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url]];
            [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
        });
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
}


-(void)fetchedData:(NSData *)responseData {
    //parse out the json data
    self.lblBalance.text = @"Balance: N/A";
    NSString *aStr =[[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
    
    if (aStr !=NULL) {
        
        
        if ([aStr rangeOfString:@"\n"].location != NSNotFound)
        {
            // NSLog(@"charecter found");
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
        }
        if ([aStr rangeOfString:@"\r"].location != NSNotFound)
        {
            // NSLog(@"charecter found");
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            
        }
        NSString *res = [NSString stringWithFormat:@"Balance : %@",aStr];
        self.lblBalance.text = res;
    }
    else{
        
        NSString *res = @"Balance: N/A";
        self.lblBalance.text = res;
        
    }
}

-(void) getAddressbookPermission{
    
    CFErrorRef error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        
        if(granted){
            NSLog(@"Addressbook permissiion granted.");
        }
        else{
            NSLog(@"%@", error);
        }
    });
}


-(void)configPushsService{
    
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *userid = [userDefault objectForKey:data_user_name];
    NSString *token = [userDefault objectForKey:data_device_token];
    
    if(userid != nil && token != nil){
        NSString *url_string = [insert_token_link stringByReplacingOccurrencesOfString:@"##userid##" withString:userid];
        url_string = [url_string stringByReplacingOccurrencesOfString:@"##token##" withString:token];
        [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
        
    }
}


@end
