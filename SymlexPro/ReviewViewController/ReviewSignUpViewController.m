
#import "ReviewSignUpViewController.h"
#import "ViewExtension.h"
#import "TextFieldUtils.h"
#import "CountryDataSource.h"
#import "UIViewController+MJPopupViewController.h"
#import "LoginManager.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "OtpViewController.h"
#import "UIView+Toast.h"
#import "MainPageViewController.h"
#import "SVProgressHUD.h"
#import "Base64.h"
#import "TabViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "MyApp.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface ReviewSignUpViewController(){

    NSString *countryCode;
    NSUserDefaults *userDefault;
    
}

@property (strong, nonatomic) NSArray *countryObjects;

@end

@implementation ReviewSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault =[NSUserDefaults standardUserDefaults];
    
    [self updateView];
    
    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];
    
    //restartPJSIP();
    
}

-(void) viewWillAppear:(BOOL)animated{
    if(userDefault != nil){
        //self.tfOppcode.text = [userDefault stringForKey:data_opcode];
        self.tfUsername.text = [userDefault stringForKey:data_user_name];
        self.tfPassword.text = [userDefault stringForKey:data_password];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

-(void) updateView{
    
    self.tfUsername.delegate=self;
    self.tfPassword.delegate=self;
    
    self.tfPassword.textColor = [UIColor whiteColor];
    self.tfUsername.textColor = [UIColor whiteColor];
    
    [self.tfUsername addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    
    [self updateLogInButton];
}

- (IBAction)btnSignUpPressed:(id)sender {
    [self.view endEditing:YES];
    
    
    [SVProgressHUD showWithStatus:@"please wait.."];
    [self getXmlConfig];
    [self dismiskeyboard];
}

- (IBAction)pressedCreateAcc:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://mdns.sipthor.net/register_sip_account.phtml"]];

}


-(void)getXmlConfig{
    
        Boolean network = [NetworkManager getNetworkStatus];
        if(network){
            GetDataController *controller = [[GetDataController alloc] init];
                controller.delegate = self;
                [controller getFromUrl:configTestURL withData:nil withTag:200];
        }
        else{
            [self.view makeToast:@"Please Check Your Internet"];
        }
}


-(void) getFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{

    NSLog(@"%@",responseObject);
    if(tag == 200){
        
        if (![responseObject isEqualToString:@"99"])
        {
            NSData	*b64DecData = [Base64 decode:responseObject];
            NSString *msg = [[NSString alloc] initWithData:b64DecData encoding:NSASCIIStringEncoding];
            XMLParser *xmlParser =[[XMLParser alloc] init];
            xmlParser.delegate = self;
            [xmlParser parseData:msg];
        }
        else{
            [self.view makeToast:@"Incorrect Operator Code"];
        }
        
        [SVProgressHUD dismiss];
    }
    
}

-(void) getFinishedWithError:(NSError *)error{

    NSLog(@"%@",error);
}

-(void) xmlParserFinishedParsingConfig{

    NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];

    NSString *sipIP = [newUser objectForKey:data_sip_ip];
    
    [userDefault removeObjectForKey:data_opcode];
    [userDefault removeObjectForKey:data_user_name];
    [userDefault removeObjectForKey:data_password];
    [userDefault setObject:_tfUsername.text forKey:data_user_name];
    [userDefault setObject:_tfPassword.text forKey:data_password];
    
//    if(sipIP != nil && ![sipIP isEqualToString:@""]){
//
//        [[self navigationController] setNavigationBarHidden:YES animated:YES];
//        [userDefault setObject:@"true" forKey:data_isAccountCreated];
//    }
    
    restartPJSIP();
    [self configPushsService];
}

- (void)updateLogInButton {
    BOOL textFieldsNonEmpty = self.tfUsername.text.length>2
     && self.tfPassword.text.length > 2;
  //  BOOL isValidEmail = [TextFieldUtils NSStringIsValidEmail:self.tfEmail.text];
    self.btnSignUp.enabled = textFieldsNonEmpty;
    
    if(textFieldsNonEmpty){
        self.btnSignUp.customTextColor = [UIColor whiteColor];
    }else{
        self.btnSignUp.customTextColor = [UIColor grayColor];
    }
}

-(void)dismiskeyboard{
    //[self.tfOppcode resignFirstResponder];
    [self.tfUsername resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    
    if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:self.tfUsername] || [sender isEqual:self.tfPassword])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


-(void)netStat:(NSNotification *)notification{
    NSLog(@"-----))))(((((-------");
    sip_set_registration();
}


-(void)configPushsService{
    
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *userid = [userDefault objectForKey:data_user_name];
    NSString *token = [userDefault objectForKey:data_device_token];
    
    if(userid != nil && token != nil){
        NSString *url_string = [insert_token_link stringByReplacingOccurrencesOfString:@"##userid##" withString:userid];
        url_string = [url_string stringByReplacingOccurrencesOfString:@"##token##" withString:token];
        [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
        
    }
}



@end
