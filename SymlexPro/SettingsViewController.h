//
//  SettingsViewController.h
//  SymlexPro
//
//  Created by admin on 9/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface SettingsViewController : UIViewController<MFMessageComposeViewControllerDelegate>
- (IBAction)tellAFriendPressed:(id)sender;
- (IBAction)dialpadPressed:(id)sender;

@end
