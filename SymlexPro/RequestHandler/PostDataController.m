//
//  PostDataController.m
//  helloworld
//
//  Created by Riaz on 6/2/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import "PostDataController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"

@implementation PostDataController

-(void) initWithURL:(NSString*) pageurl withData:(NSDictionary *)data{
    
    [SVProgressHUD showWithStatus:@"please wait.."];
    NSString *url = [NSString stringWithFormat:@"%@",pageurl];
    //send basic info
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithData:)]) {
            [self.delegate postFinishedWithData:responseObject];
            
        }
        
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
         [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithError:)]) {
            [self.delegate postFinishedWithError:error];
        }
    }];
    
    
}



-(void) uploadDataWithImage:(NSString*) pageurl withData:(NSDictionary *)data withImage:(UIImage*) image{
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, .5);
    
    
    NSString *url = [NSString stringWithFormat:@"%@",pageurl];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:url parameters:data constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"prescriptionImage.JPG" mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithData:)]) {
            [self.delegate postFinishedWithData:responseObject];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithError:)]) {
            [self.delegate postFinishedWithError:error];
        }

    }];
}


-(void) postwithURL:(NSString*) pageurl withData:(id)data withTag:(NSUInteger)tag{
    
    [SVProgressHUD showWithStatus:@"please wait.."];
    NSString *url = [NSString stringWithFormat:@"%@",pageurl];
    //send basic info
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithData: withTag:)]) {
            [self.delegate postFinishedWithData:responseObject withTag:tag];
            
        }
        
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithError:)]) {
            [self.delegate postFinishedWithError:error];
        }
    }];
    
}


-(void) uploadImageWithURL:(NSString*) pageurl withData:(NSDictionary *)data withImage:(UIImage*) image withTag:(NSUInteger) tag{
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, .5);
    
    
    NSString *url = [NSString stringWithFormat:@"%@",pageurl];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:url parameters:data constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.JPG" mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithData: withTag:)]) {
            [self.delegate postFinishedWithData:responseObject withTag:tag];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(postFinishedWithError:)]) {
            [self.delegate postFinishedWithError:error];
        }
        
    }];
}

@end
