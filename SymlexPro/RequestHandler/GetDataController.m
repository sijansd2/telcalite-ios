//
//  GetDataController.m
//  helloworld
//
//  Created by Riaz on 6/2/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import "GetDataController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"

@implementation GetDataController{

    UIAlertController *pleaseWait;
}




-(void) initWithURL:(NSString*) pageurl withData:(NSDictionary *)data{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSString *url = [NSString stringWithFormat:@"%@",pageurl];
    
    [manager GET:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(getFinishedWithData:)]) {
            [self.delegate getFinishedWithData:responseObject];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
     
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(getFinishedWithError:)]) {
            [self.delegate getFinishedWithError:error];
        }
    }];
    
    
}

-(void) getFromUrl:(NSString*) pageurl withData:(NSDictionary *)data withTag:(NSUInteger)tag{
    
    
    [SVProgressHUD showWithStatus:@"please wait.."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    
    NSString *url = [NSString stringWithFormat:@"%@",pageurl];
    
    
    [manager GET:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [SVProgressHUD dismiss];
        NSLog(@"%@",responseObject);
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];

        if (self.delegate && [self.delegate respondsToSelector:@selector(getFinishedWithData: withTag:)]) {
            [self.delegate getFinishedWithData:string withTag:tag];
        }
        

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"%@",error);
        if (self.delegate && [self.delegate respondsToSelector:@selector(getFinishedWithError:)]) {
            [self.delegate getFinishedWithError:error];
        }
    }];
    
}

-(void) postDataAsGet:(NSString*) pageurl withData:(NSDictionary *)data{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *url = [NSString stringWithFormat:@"%@",pageurl];
    
    
    [manager GET:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        

        if (self.delegate && [self.delegate respondsToSelector:@selector(postDataAsGetFinished:)]) {
            [self.delegate postDataAsGetFinished:responseObject];
        }
        
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
            
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(getFinishedWithError:)]) {
            [self.delegate getFinishedWithError:error];
        }
    }];
    
    
}

@end
