
#import "SettingsViewController.h"
#import "DialpadViewController.h"
#import "Constans.h"
#import "MyApp.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //register an observer for registration callback coming from myapp.m
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processRegState:)
                                                 name: kSIPRegState
                                               object:nil];
    
    [self initStatusButton:@"red_status"];
}

-(void) viewDidAppear:(BOOL)animated{
    
    get_account_status();
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initStatusButton :(NSString *) imageName{
    
    UIImage *image = [UIImage imageNamed:imageName];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,15, 15)];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.adjustsImageWhenHighlighted = NO;
    UIBarButtonItem *leftButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}


- (void)processRegState:(NSNotification *)notification
{
    NSDictionary *dictionary = [notification userInfo];
    
    //  NSLog(@"Status in phoneview: %d",[[dictionary objectForKey:@"Status"] intValue]);
    if ([[dictionary objectForKey:@"Status"] intValue] == 200){
        [self initStatusButton:@"green_status"];
    }
    else if ([[dictionary objectForKey:@"StatusText"] isEqual:@"registersent"]){
        [self initStatusButton:@"yellow_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 403){
        [self initStatusButton:@"red_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 404){
        [self initStatusButton:@"red_status"];
    }else if ([[dictionary objectForKey:@"Status"] intValue] == 407){
        [self initStatusButton:@"red_status"];
    }
    
    else{
        [self initStatusButton:@"red_status"];
    }
    
}


- (IBAction)tellAFriendPressed:(id)sender {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.recipients = [NSArray arrayWithObjects:nil, nil];
        controller.messageComposeDelegate = self;
        controller.body = @"Lets make free and cheap international call from Zuniro. Download Zuniro from https://itunes.apple.com/us/app/zuniro/id1289551870?ls=1&mt=8";
        [self presentViewController:controller animated:YES completion:nil];
    }
    
}

- (IBAction)dialpadPressed:(id)sender {
    DialpadViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
    [self presentViewController:viewController animated:YES completion:nil];
}

-(void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    if (result == MessageComposeResultCancelled){
        NSLog(@"Message cancelled");
    }
    else if (result == MessageComposeResultSent){
        NSLog(@"Message sent");
    }
    else{
        NSLog(@"Message failed");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
