//
//  SignInViewController.h
//  SymlexPro
//
//  Created by admin on 7/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerConfig.h"
#import "ComboView.h"

@interface SignInViewController : UIViewController< ServerCongigDelegate,ComboDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oppcode;
@property (weak, nonatomic) IBOutlet UITextField *tfNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)signinPressed:(id)sender;

@end
