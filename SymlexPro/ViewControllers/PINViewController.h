//
//  PINViewController.h
//  SymlexPro
//
//  Created by USER on 9/14/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataController.h"

@interface PINViewController : UIViewController<PostDataControllerDelegate>

@property(nonatomic,weak) IBOutlet UITextField *tfCurrentPin;
@property(nonatomic,weak) IBOutlet UITextField *tfNewPin;
@property(nonatomic,weak) IBOutlet UITextField *tfConfirmPin;
@property(nonatomic,weak) IBOutlet UILabel *lbFrontMessage;
@property(assign,nonatomic) Boolean isFirstTime;
@property(nonatomic,weak) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentPinHeightConstraint;

-(IBAction) btnSavePressed :(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;

@end
