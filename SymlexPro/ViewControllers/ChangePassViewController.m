

#import "ChangePassViewController.h"
#import "Constans.h"
#import "MyEncryption.h"

@interface ChangePassViewController ()

@end

@implementation ChangePassViewController

UIActivityIndicatorView *indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
        //Tap gesture enable
//    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
//    [self.view addGestureRecognizer:tap];
//    
//    //waiting view
//    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
//    indicator.center = self.view.center;
//    [self.view addSubview:indicator];
//    [indicator bringSubviewToFront:self.view];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


//MARK: Functions
-(void)changePassword{
    
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,changePass];
    NSString* md5Str = [@"8801686019992123456" MD5String];
    NSDictionary *param = @{@"username": @"8801686019992",
                            @"password": @"123456",
                            @"api_key": @"12985c70637f5775530bf704cdfa7b4a",
                            @"sec_key": md5Str,
                            @"old_password": @"1234567",
                            @"new_password": @"1234567"};
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    [controller initWithURL:url withData:param];
}


-(void)forgotPassword{
    
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,forgetPassword];
    NSDictionary *param = @{@"email": @"sijan@gmail.com",
                            @"api_key": @"12985c70637f5775530bf704cdfa7b4a"};
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    [controller initWithURL:url withData:param];
}

-(void)dismiskeyboard{
    [self.tfOldPass resignFirstResponder];
    [self.tfNewPass resignFirstResponder];
    [self.tfConfirmPass resignFirstResponder];
}



//MARK: Http post callbacks
-(void)postFinishedWithData:(id)responseObject{
    NSString *msg = [responseObject objectForKey:@"message"];
    [self showAlert:@"Server Response" messageBody:msg];
    [indicator stopAnimating];
}

-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    NSLog(@"Server response : postFinishedWithData");
    [indicator stopAnimating];
}


-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"%@",error);
    [self showAlert:@"Error" messageBody:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    [indicator stopAnimating];
}


//MARK: Alert
-(void) showAlert:(NSString*)title messageBody:(NSString*)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Server Response" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    //NSLog(@"you pressed Yes button");
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


// MARK: Pressed Function
- (IBAction)forgotBtnPressed:(id)sender {
    
}

- (IBAction)savePressed:(id)sender {
    [self dismiskeyboard];
    [self changePassword];
    [indicator startAnimating];
}

- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [indicator stopAnimating];
    [self dismiskeyboard];
}
@end
