
#import "SignInViewController.h"
#import "MyEncryption.h"
#import "Constans.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "ServerConfig.h"
#import "ViewExtension.h"
#import "TextFieldUtils.h"
#import "MainPageViewController.h"
#import "SVProgressHUD.h"
#import "UIViewController+MJPopupViewController.h"
#import "CountryDataSource.h"
#import "SetPinView.h"
#import "KLCPopup.h"

NSString* phnNumber;
NSString* password;


@interface SignInViewController (){

    NSString *countryCode;
}
@property (strong, nonatomic) NSArray *countryObjects;

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setBackGroundImage:@"sign_in_background.jpg"];
    [self updateView];
    
    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];
}


-(void) updateView{
     [TextFieldUtils addImageLeftSideTextView:self.oppcode imageName:@"flag.png"];
     [TextFieldUtils addImageRightSideTextView:self.oppcode imageName:@"arrow_right.png"];
     [TextFieldUtils addImageLeftSideTextView:self.tfNumber imageName:@"phone.png"];
     [TextFieldUtils addImageLeftSideTextView:self.tfPassword imageName:@"password.png"];
    
    self.oppcode.delegate = self;
    self.oppcode.textColor = [UIColor whiteColor];
    self.tfPassword.textColor = [UIColor whiteColor];
    self.tfNumber.textColor = [UIColor whiteColor];
    
    [self.tfNumber addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self updateLogInButton];
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.oppcode){
        
        [self.view endEditing:YES];
        ComboView *popupview = [self.storyboard instantiateViewControllerWithIdentifier:@"comboview"];
        popupview.delegate = self;
        popupview.comboType = @"Country";
        popupview.comboNameProperty = @"name";
        popupview.comboArray=[self allCountryObjects];
        [self presentPopupViewController:popupview animationType:MJPopupViewAnimationFade];
        return NO;
    }
    
    return YES;
    
}
-(void)userFinishedChoosingitem:(NSDictionary *)item andType:(NSString *)type{
    
    
    self.oppcode.text = item[@"name"];
    countryCode = item[@"code"];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    CGSize stringBoundingBox = [countryCode sizeWithFont:font];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35+stringBoundingBox.width, 40)];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 24, 24)];
    imageview.image = [UIImage imageNamed:@"phone.png"];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    self.tfNumber.leftViewMode = UITextFieldViewModeAlways;
    [view addSubview:imageview];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, stringBoundingBox.width, 39)];
    label.text = countryCode;
    label.textColor=[UIColor whiteColor];
    label.font = font;
    [view addSubview:label];
    self.tfNumber.leftViewMode = UITextFieldViewModeAlways;
    self.tfNumber.leftView = view;
    [self.tfNumber clipsToBounds];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

-(void) closeButtonClicked{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}


- (NSArray *)allCountryObjects
{
    if(!self.countryObjects){
        CountryDataSource *c = [[CountryDataSource alloc] init];
        NSMutableArray *countryNames = [c getCountries];
        [self setCountryObjects:[NSArray arrayWithArray:countryNames]];
    }
    
    return self.countryObjects;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//MARK: Functions
-(void)checkUser{
    
    NSString* num = phnNumber;
    NSString* api_key = apiKey;
    NSString* url = [NSString stringWithFormat: @"%@%@%@%@%@%@",baseUrl,checkUser,@"/",num,@"/",api_key];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [SVProgressHUD showWithStatus:@"Checking User..."];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [SVProgressHUD dismiss];
        NSLog(@"%@",responseObject);
        NSString* msg = [responseObject objectForKey:@"message"];
        if([msg isEqualToString:@"success"]){
            [self checkPin];
        }else{
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [SVProgressHUD dismiss];
        NSLog(@"%@",error);
        [self showAlert:@"Error" messageBody:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    }];
}

-(void)checkPin{
    
    [SVProgressHUD showWithStatus:@"Checking if pin exists.."];
    
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,checkPin];
    NSString* md5Str = [[NSString stringWithFormat:@"%@%@",phnNumber,password] MD5String];
    NSDictionary *param = @{@"username": phnNumber,
                            @"password": password,
                            @"api_key": apiKey,
                            @"sec_key": md5Str};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [SVProgressHUD dismiss];
        NSLog(@"%@",responseObject);
        NSString* msg = [responseObject objectForKey:@"message"];
        if(![msg isEqualToString:@"Pin already Set"]){
            [self showPinView];
        }else{
  
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
        [self showAlert:@"Error" messageBody:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    }];
}


-(void) showPinView{

    [self.view endEditing:YES];
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SetPinView" owner:self options:nil];
    SetPinView *postview = [nibObjects objectAtIndex:0];
    postview.frame = CGRectMake(0, 0, self.view.frame.size.width*.8, 220);
    
    KLCPopupLayout layout = KLCPopupLayoutMake((KLCPopupHorizontalLayout) KLCPopupHorizontalLayoutCenter,
                                               (KLCPopupVerticalLayout) KLCPopupVerticalLayoutAboveCenter);
    
    KLCPopup* popup = [KLCPopup popupWithContentView:postview
                                            showType:(KLCPopupShowType)KLCPopupShowTypeFadeIn
                                         dismissType:(KLCPopupDismissType)KLCPopupDismissTypeNone
                                            maskType:(KLCPopupMaskType)KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:NO
                               dismissOnContentTouch:NO];
    
    
    [popup showWithLayout:layout];
}

-(void)getConfigurationFromServer{
    [SVProgressHUD showWithStatus:@"Getting Configuration..."];
    NSString* url = @"http://199.33.126.83/ad_sha/ad_sx.php?as=7777";
    dispatch_async(kBgQueues, ^{
        NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url]];
        [self performSelectorOnMainThread:@selector(aaa:) withObject:data waitUntilDone:YES];
    });

}

-(void)aaa:(NSData*)data{
    [SVProgressHUD dismiss];
    ServerConfig* srvCfg = [[ServerConfig alloc] init];
    srvCfg.srvDelegate = self;
    [srvCfg fetchedData:data];
}


-(void)successfullyFetchedData{
    [self checkUser];
}


//MARK: Alert
-(void) showAlert:(NSString*)title messageBody:(NSString*)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    //NSLog(@"you pressed Yes button");
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


//MARK: Pressed Functions
- (IBAction)signinPressed:(id)sender {
    phnNumber = [self phoneNumberWithCCode:countryCode withNumber:self.tfNumber.text];
    password = self.tfPassword.text;
     [self showPinView];
   // [self getConfigurationFromServer];
}

- (IBAction)newAccPressed:(id)sender {
    
}

-(NSString *) phoneNumberWithCCode:(NSString *) code withNumber:(NSString *) phoneNumber{
    return [self removeUnnecssaryCharacter:[NSString stringWithFormat:@"%@%@",code,phoneNumber]];;
}
- (NSString *)removeUnnecssaryCharacter:(NSString *)string {
    
    NSString * strippedNumber = [string stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [string length])];
    
    return strippedNumber;
}
- (void)updateLogInButton {
    BOOL textFieldsNonEmpty = self.tfNumber.text.length > 0 && self.tfPassword.text.length > 0 && self.oppcode.text.length > 0;
    self.btnLogin.enabled = textFieldsNonEmpty;
}

- (IBAction)btnBackPressed:(id)sender {
    
    MainPageViewController *viewController = [self.navigationController.viewControllers objectAtIndex:0];
    [self.navigationController popToViewController:viewController animated:YES];
    
}

-(void)dismiskeyboard{
    [self.oppcode resignFirstResponder];
    [self.tfNumber resignFirstResponder];
    [self.tfPassword resignFirstResponder];
}
@end
