//
//  VoucherRechargeViewController.h
//  SymlexPro
//
//  Created by admin on 7/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataController.h"

@interface VoucherRechargeViewController : UIViewController<PostDataControllerDelegate>
- (IBAction)cancelPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tfPinNumber;
- (IBAction)rechargePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRecharge;

@end
