//
//  ContactDetailViewController.h
//  SymlexPro
//
//  Created by admin on 8/2/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
#import <MessageUI/MFMessageComposeViewController.h>
@interface ContactDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileBottomConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgCover;
@property (weak, nonatomic) IBOutlet UILabel *lbUserName;
@property (weak, nonatomic) IBOutlet UITableView *phoneNoTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property(strong,nonatomic) Person *person;
@property (weak, nonatomic) IBOutlet UIView *headerView;
-(IBAction) btnDialpadPressed:(id)sender;
@end
