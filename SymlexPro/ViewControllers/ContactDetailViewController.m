//
//  ContactDetailViewController.m
//  SymlexPro
//
//  Created by admin on 8/2/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "ContactDetailViewController.h"
#import "DialpadViewController.h"
#import "CallViewController.h"
#import "MyApp.h"
#import "Constans.h"

@interface ContactDetailViewController (){

    NSArray *cellNames;
    NSArray *cellImages;
    NSMutableArray * arrayForBool;
}

@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayForBool = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[ self.person.phoneNumbers count]; i++) {
        [ arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    
    cellNames = [[NSArray alloc] initWithObjects:@"Free Call",@"Low Cost Call",@"Cellular Call",@"Celluar Message", nil];
    cellImages = [[NSArray alloc] initWithObjects:@"call_white",@"call_white",@"call_white",@"message", nil];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self setUpUI];
    // Do any additional setup after loading the view.
}

-(void) setUpUI{

    if(self.person == nil)
        return;
    
    self.lbUserName.text = self.person.fullName;
    self.imgProfile.image = self.person.image;
    self.imgCover.image = self.person.image;
    [self.imgCover setAlpha:0.5];
    
}
-(void) viewDidLayoutSubviews{

    [super viewDidLayoutSubviews];
    
    CGRect frame = self.phoneNoTable.tableHeaderView.frame;
    frame.size.height=self.view.frame.size.height*.4;
    self.phoneNoTable.tableHeaderView.frame = frame;
    
    
    [self.headerView layoutIfNeeded];
    
    float height = self.imgProfile.layer.frame.size.height;
    self.imgProfileBottomConstraint.constant = height/2;
    self.imgProfile.layer.cornerRadius = height/2;
    self.imgProfile.layer.borderWidth = 2;
    self.imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgProfile.clipsToBounds = YES;
    self.imgProfile.layer.masksToBounds = YES;
    //[self.headerView layoutSubviews];
    //[self.imgProfile setNeedsUpdateConstraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.person.phoneNumbers.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return cellNames.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImageView *leftImage = [cell viewWithTag:101];
    UILabel *textLabel = [cell viewWithTag:102];

    leftImage.image = [UIImage imageNamed:cellImages[indexPath.row]];
    leftImage.layer.cornerRadius = 15;
    leftImage.clipsToBounds = YES;
    leftImage.layer.masksToBounds = YES;
    
    textLabel.text = cellNames[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        return 50;
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString *CellIdentifier = @"Header";
    
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UILabel *sectionName = [headerView viewWithTag:200];
    sectionName.text =self.person.phoneNumbers[section];
    
    
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.phoneNoTable.frame.size.width,50)];
    sectionView.tag=section;
    
    headerView.frame = sectionView.frame;
    [sectionView addSubview:headerView];
    
    BOOL collapsed  = [[ arrayForBool objectAtIndex:section] boolValue];
    if(collapsed){
    
    }
    
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    
    
    
    return  sectionView;
    
    
}


pjsua_acc_id acc_id = 0;
pjsua_acc_info infos;

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSLog(@"%ld---%ld",indexPath.section,indexPath.row);
    NSString *number = self.person.phoneNumbers[indexPath.section];
    
    switch (indexPath.row) {
        case 0:
            
            pjsua_acc_get_info(acc_id,&infos);
            //check if number is available in dialpadtext
            if(number.length >0){
                //check current registration status
                if(infos.status==200 && infos.expires>0){
                    
                    CallViewController *callView = [self.storyboard instantiateViewControllerWithIdentifier:@"callviewcontrollerId"];
                    callView.phoneNumber = number;
                    callView.callType=callTypeFree;
                    [self presentViewController:callView animated:YES completion:nil];
                    
                }
            }else{
                
                NSLog(@"No number pressed");
                
            }
            break;
        case 1:{
            DialpadViewController *viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
            viewcontroller.phoneNumber = number;
            [self presentViewController:viewcontroller animated:YES completion:nil];
            
        }
            break;
        case 2:
        {
            UIApplication *application = [UIApplication sharedApplication];
            [application openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",number]] options:@{} completionHandler:nil];
        }
            break;
        case 3:
        {
//            UIApplication *application = [UIApplication sharedApplication];
//            [application openURL:[NSURL URLWithString:[NSString stringWithFormat:@"sms:%@",number]] options:@{} completionHandler:nil];
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            if([MFMessageComposeViewController canSendText])
            {
                controller.recipients = [NSArray arrayWithObjects:number, nil];
                controller.messageComposeDelegate = self;
                [self presentViewController:controller animated:YES completion:nil];
            }
        }

            break;
            
        default:
            break;
    }
    [self.phoneNoTable deselectRowAtIndexPath:indexPath animated:NO];
}

-(void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    if (result == MessageComposeResultCancelled){
                NSLog(@"Message cancelled");
            }
            else if (result == MessageComposeResultSent){
                NSLog(@"Message sent");
            }
            else{
                NSLog(@"Message failed");
            }
            [self dismissViewControllerAnimated:YES completion:nil];
}

//-(void)mailComposeController:(MFMessageComposeViewController *)controller
//         didFinishWithResult:(MessageComposeResult)result error:(NSError *)error
//{
//    if (result == MessageComposeResultCancelled){
//        NSLog(@"Message cancelled");
//    }
//    else if (result == MessageComposeResultSent){
//        NSLog(@"Message sent");
//    }
//    else{
//        NSLog(@"Message failed");
//    }
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];


    if (indexPath.row == 0) {
        BOOL collapsed  = [[ arrayForBool objectAtIndex:indexPath.section] boolValue];

        for (int i=0; i<[ self.person.phoneNumbers count]; i++) {
            if (indexPath.section==i) {
//                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
//                [self.phoneNoTable reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];

                NSString *number = self.person.phoneNumbers[indexPath.section];
                if(number){
                    DialpadViewController *viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
                    dialingNumber = number;
                    self.tabBarController.selectedIndex = 1;
                }
                
                if(!collapsed){

                }

            }else{
//                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
//                [self.phoneNoTable reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }

    }
    
    
    
    
}


@end
