
#import "VoucherRechargeViewController.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "NetworkManager.h"
#import "UIView+Toast.h"

@interface VoucherRechargeViewController (){
    
    NSUserDefaults *userDefault;
}

@end

@implementation VoucherRechargeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];

    [self updateView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


//MARK: Http post callbacks
-(void)postFinishedWithData:(id)responseObject{
    NSString *msg = [responseObject objectForKey:@"message"];
    [self showVoucherAlert:@"Server Response" messageBody:msg];
    
}

-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    NSLog(@"Server response : postFinishedWithData");
    
}


-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"%@",error);
    [self showVoucherAlert:@"Error" messageBody:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    
}


//MARK: Alert
-(void) showVoucherAlert:(NSString*)title messageBody:(NSString*)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Server Response" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    //NSLog(@"you pressed Yes button");
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


//MARK: Preessed Functions
- (IBAction)cancelPressed:(id)sender {
    [self dismiskeyboard];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)rechargePressed:(id)sender {

    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        [self dismiskeyboard];
        [self voucharRecharge];
        
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    
}


//MARK: Functions
-(void)voucharRecharge{
    NSString* user = [userDefault stringForKey:data_user_name];
    NSString* pass = [userDefault stringForKey:data_password];
    NSString* pinNum = self.tfPinNumber.text;
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,voucherRecharge];
    NSString* md5Str = [[NSString stringWithFormat:@"%@%@",user,pass] MD5String];
    NSDictionary *param = @{@"username": user,
                            @"password": pass,
                            @"api_key": apiKey,
                            @"sec_key": md5Str,
                            @"pin_number": pinNum};
    
    if(![pinNum isEqualToString:@""]){
        PostDataController *controller = [[PostDataController alloc] init];
        controller.delegate = self;
        [controller initWithURL:url withData:param];
    }
    else{
        [self.view makeToast:@"Pin Code field is empty"];
    }
    
    
    
}

-(void)dismiskeyboard{
    [self.tfPinNumber resignFirstResponder];
}

-(void)updateView{
    self.btnRecharge.enabled = false;
    [self.tfPinNumber addTarget:self action:@selector(updateRechargeButton) forControlEvents:UIControlEventEditingChanged];
}

-(void)updateRechargeButton{
    BOOL textFieldsIsValid = self.tfPinNumber.text.length > 0;
    self.btnRecharge.enabled = textFieldsIsValid;
}

@end
