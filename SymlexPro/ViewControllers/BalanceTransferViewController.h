//
//  BalanceTransferViewController.h
//  SymlexPro
//
//  Created by admin on 7/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataController.h"

@interface BalanceTransferViewController : UIViewController<PostDataControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfToNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet UITextField *tfSecretPin;
- (IBAction)transferPressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTransfer;

@end
