//
//  CallLogDetailsViewController.m
//  SymlexPro
//
//  Created by USER on 9/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "CallLogDetailsViewController.h"
#import "LogDetailTableViewCell.h"
@interface CallLogDetailsViewController (){
    NSMutableArray *allCallLogs;
}

@end

@implementation CallLogDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    allCallLogs = [[NSMutableArray alloc] init];
    
    self.name.text = self.logUser.name;
    self.profilePhoto.image = self.logUser.image;
    self.coverPhoto.image = self.logUser.image;
    self.profilePhoto.layer.cornerRadius = self.profilePhoto.layer.frame.size.width/2;
    self.profilePhoto.clipsToBounds = YES;
    self.profilePhoto.layer.masksToBounds = YES;
    [self loadData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadData{
    
    allCallLogs = [[NSMutableArray alloc] init];
    
    allCallLogs =[[NSMutableArray alloc] initWithArray:[[DBManager getSharedInstance] getAllCallLogsByNumber:self.logUser.number]];
    
    NSLog(@"count---%ld",allCallLogs.count);
    [self.tableView reloadData];
}



//MARK: TableView Callbacks
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LogDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    LogsHolder *temp =(LogsHolder*)allCallLogs[indexPath.row];
    cell.lbTime.text = [self getFormattedTime:temp];
    cell.lbCallStatus.text = [NSString stringWithFormat:@"%@,%@",temp.call_status,temp.Duration];
    
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return allCallLogs.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}


-(NSString *) getDuration:(NSString *) seconds{
    
    NSInteger interval = [seconds integerValue];
    NSInteger second = interval % 60;
    NSInteger minutes = (interval / 60) % 60;
    NSInteger hours = (interval / 3600);
    return [NSString stringWithFormat:@"%02ld hrs %02ld mins %02ld secs", (long)hours, (long)minutes, (long)second];
}


-(NSString *) getFormattedTime:(LogsHolder *) log{

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setLocale:[NSLocale currentLocale]];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDate *olddate = [dateFormat dateFromString:log.date];
    NSDate *today = [NSDate date];
    
    NSString *formattedDate=@"";
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    long differenceInDays =
    [calendar ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitEra forDate:olddate] -[calendar ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitEra forDate:today];
    
    switch (differenceInDays) {
        case -1:
            [dateFormat setDateFormat:@"hh:mm a"];
            formattedDate = [NSString stringWithFormat:@"Yesterday %@",[dateFormat stringFromDate:olddate]];
            break;
        case 0:
            [dateFormat setDateFormat:@"hh:mm a"];
            formattedDate = [NSString stringWithFormat:@"Today %@",[dateFormat stringFromDate:olddate]];
            break;
        case 1:
            [dateFormat setDateFormat:@"hh:mm a"];
            formattedDate = [NSString stringWithFormat:@"Tomorrow %@",[dateFormat stringFromDate:olddate]];
            break;
        default: {
           [dateFormat setDateFormat:@"dd MMM hh:mm a"];
            formattedDate = [dateFormat stringFromDate: olddate];
            break;
        }
    }
    
    return formattedDate;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
