//
//  TabViewController.m
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "TabViewController.h"
#import "ColorUtility.h"
#import "MyApp.h"
#import "AppDelegate.h"
#import "Constans.h"
#import "AFNetworking.h"

@interface TabViewController ()

@end

@implementation TabViewController
NSString *remoteHostName = @"www.google.com";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NetworkManager *manager = [NetworkManager getInstance];
    // Do any additional setup after loading the view.
    
    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *item3 = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *item4 = [self.tabBar.items objectAtIndex:3];
    
    
    //adding tab images in tab bar controller
    item1.image = [[UIImage imageNamed:@"contacts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item1.selectedImage = [[UIImage imageNamed:@"contact_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item2.image = [[UIImage imageNamed:@"dialpad_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item2.selectedImage = [[UIImage imageNamed:@"dialpad_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item3.image = [[UIImage imageNamed:@"calls"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item3.selectedImage = [[UIImage imageNamed:@"call_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item4.image = [[UIImage imageNamed:@"settings"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item4.selectedImage = [[UIImage imageNamed:@"settings_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor] }
                                             forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [ColorUtility colorWithHexString:textColor] }
                                            forState:UIControlStateSelected];
    
    //[[self tabBar] setTintColor:[UIColor darkGrayColor]];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(netStat:)
                                                 name: NetworkUpdate
                                               object:nil];
    
    restartPJSIP();
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *userdef =[NSUserDefaults standardUserDefaults];
    if(self.isFirstLaunch){
        if(![[userdef objectForKey:@"isIpv6"] isEqualToString:@"true"]){
            [self setSelectedIndex:1];
            self.isFirstLaunch = FALSE;
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)netStat:(NSNotification *)notification{
    NSLog(@"-----))))(((((-------");
    sip_set_registration();
}

@end
