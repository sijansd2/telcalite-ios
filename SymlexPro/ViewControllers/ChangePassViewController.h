//
//  ChangePassViewController.h
//  SymlexPro
//
//  Created by admin on 7/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataController.h"

@interface ChangePassViewController : UIViewController<PostDataControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfOldPass;
@property (weak, nonatomic) IBOutlet UITextField *tfNewPass;
@property (weak, nonatomic) IBOutlet UITextField *tfConfirmPass;
- (IBAction)forgotBtnPressed:(id)sender;
- (IBAction)savePressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;

@end
