//
//  ResetPinViewController.h
//  SymlexPro
//
//  Created by admin on 7/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataController.h"

@interface ResetPinViewController : UIViewController<PostDataControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfCurrentPin;
@property (weak, nonatomic) IBOutlet UITextField *tfNewPin;
@property (weak, nonatomic) IBOutlet UITextField *tfConfirmPin;
- (IBAction)savePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end
