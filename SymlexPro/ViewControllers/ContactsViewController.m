
#import "ContactsViewController.h"
#import "Person.h"
#import "ContactDetailViewController.h"
#import "DialpadViewController.h"
#import "Constans.h"
#import "MyApp.h"

@interface ContactsViewController (){

    NSArray *alphabets;
    NSMutableArray *AllFilteredContacts;
    NSMutableArray *sectionsArray;
    
}

@property (nonatomic, strong) NSMutableArray *tableData;
@property (nonatomic, strong) NSMutableArray *filteredTableData;

@end

@implementation ContactsViewController
ABAddressBookRef addressBook;
@synthesize searchBar,isFiltered;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setAddressBook];
    self.searchBar.delegate = self;
    self.searchBar.enablesReturnKeyAutomatically = NO;

    
    //register an observer for registration callback coming from myapp.m
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processRegState:)
                                                 name: kSIPRegState
                                               object:nil];
    
    [self initStatusButton:@"red_status"];
    
    }

-(void) viewDidAppear:(BOOL)animated{

    get_account_status();
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initStatusButton :(NSString *) imageName{

    UIImage *image = [UIImage imageNamed:imageName];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,15, 15)];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.adjustsImageWhenHighlighted = NO;
    UIBarButtonItem *leftButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftButton;

}
- (void)processRegState:(NSNotification *)notification
{
    NSDictionary *dictionary = [notification userInfo];
    
    //  NSLog(@"Status in phoneview: %d",[[dictionary objectForKey:@"Status"] intValue]);
    if ([[dictionary objectForKey:@"Status"] intValue] == 200){
       [self initStatusButton:@"green_status"];
    }
    else if ([[dictionary objectForKey:@"StatusText"] isEqual:@"registersent"]){
        [self initStatusButton:@"yellow_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 403){
        [self initStatusButton:@"red_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 404){
        [self initStatusButton:@"red_status"];
    }else if ([[dictionary objectForKey:@"Status"] intValue] == 407){
        [self initStatusButton:@"red_status"];
    }
    
    else{
        [self initStatusButton:@"red_status"];
    }
   
}
//MARK: Functions
-(void)setAddressBook{
    
    alphabets = [[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"#", nil];
    
    AllFilteredContacts = [[NSMutableArray alloc] init];
    sectionsArray = [[NSMutableArray alloc] init];
    
    addressBook= ABAddressBookCreateWithOptions(nil, nil);
    self.tableData = [[NSMutableArray alloc] init];
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            
            if (granted) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self getPersonOutOfAddressBook];
                });
            }
            
        });
        
        
    }else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self getPersonOutOfAddressBook];
        });
        
        
    }

}


- (void)getPersonOutOfAddressBook
{
    
    //NSMutableArray *allPerson = [[NSMutableArray alloc] init];
    if (addressBook != nil)
    {
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        
        CFRelease(source);
        
        NSUInteger n=[allContacts count];
        NSUInteger i = 0;
        for (i = 0; i <n; i++)
        {
            Person *person = [[Person alloc] init];
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
            NSString *lastName =  (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
            if(firstName ==NULL){
                
                firstName =@"";
            }
            if(lastName ==NULL){
                
                lastName =@"";
            }
            NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            
            person.firstName = firstName;
            person.lastName = lastName;
            person.fullName =  [fullName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
            
            ABMultiValueRef phones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
            
            NSMutableArray *temp =[[NSMutableArray alloc] init];
            for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
            {
                NSString *PhoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, j));
                PhoneNumber= [self removeUnnecssaryCharacter:PhoneNumber];
                [temp addObject:PhoneNumber];
            }
            
            person.phoneNumbers = temp;
            if([firstName isEqualToString:@""] && [lastName isEqualToString:@""] && person.phoneNumbers.count >0){
                person.fullName = person.phoneNumbers[0];
            }
            
            if (ABPersonHasImageData(contactPerson)) {
                
                    person.image= [UIImage imageWithData:(__bridge NSData * _Nonnull)(ABPersonCopyImageDataWithFormat(contactPerson, kABPersonImageFormatOriginalSize))];
                
                
            }else{
                person.image =[UIImage imageNamed: @"new_contact"];
            }
            
            
            
            [self.tableData addObject:person];
        }
    }
    
    for (int j =0; j<alphabets.count-1; j++) {
        
        NSString *c = alphabets[j];
        
        NSPredicate *pred =[NSPredicate predicateWithFormat:@"self.fullName beginswith[c] %@", c];
        NSArray *filteredContacts = [self.tableData filteredArrayUsingPredicate:pred];
        
        if(filteredContacts.count !=0){
            [AllFilteredContacts addObject:filteredContacts];
            [sectionsArray addObject:c];
        }else{
            
            [AllFilteredContacts addObject:[[NSMutableArray alloc] init]];
            [sectionsArray addObject:@""];
        }
        
        
    }
    
    
    NSPredicate *pred =[NSPredicate predicateWithFormat:@"self.fullName MATCHES %@", @"[^a-zA-Z].*"];
    NSArray *filteredContacts = [self.tableData filteredArrayUsingPredicate:pred];
    
    if(filteredContacts.count !=0){
        [AllFilteredContacts addObject:filteredContacts];
    }else{
        [AllFilteredContacts addObject:[[NSMutableArray alloc] init]];
    }
    [sectionsArray addObject:@"#"];
    
    if(addressBook!=nil){
        CFRelease(addressBook);
    }
    [self.contactsTable reloadData];
}



//MARK: TableView Callbacks
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Person *person;
    NSArray *filteredContacts;

    if(isFiltered){
    
        person = self.filteredTableData[indexPath.row];
    }else{
        filteredContacts = AllFilteredContacts[indexPath.section];
        person = filteredContacts[indexPath.row];
    }
    
    UILabel *name = (UILabel *)[cell viewWithTag:102];
    UIImageView *images =(UIImageView*)[cell viewWithTag:101];
    
    name.text= person.fullName;
    //images.image = [UIImage imageNamed:@"contacts"];
    images.image = person.image;
    images.layer.cornerRadius = 20;
    images.clipsToBounds = YES;
    images.layer.masksToBounds = YES;
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(isFiltered){
    
        return self.filteredTableData.count;
    }
    
    
    NSArray *filteredArray = AllFilteredContacts[section];
    return filteredArray.count;
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if(isFiltered){
    
        return 0;
    }
    if([sectionsArray[section] isEqualToString:@""]){
        
        return 0;
    }
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    if(isFiltered){
    
        return nil;
    }
    if([sectionsArray[section] isEqualToString:@""]){
    
        return nil;
    }
    
    return sectionsArray[section];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{

    if(isFiltered){
    
        return 1;
    }
    return sectionsArray.count;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(isFiltered){
    
        return nil;
    }
    return alphabets;
}

//MARK: Search deleget Functions
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)text{
    
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        self.filteredTableData = [[NSMutableArray alloc] init];
        
        for (Person* person in self.tableData)
        {
            NSRange nameRange = [person.fullName rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound)
            {
                [self.filteredTableData addObject:person];
            }
        }
    }
    [self.contactsTable reloadData];
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSArray *indexPaths =[self.contactsTable indexPathsForSelectedRows];
    
    NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
    [self.contactsTable deselectRowAtIndexPath:indexPath animated:YES];
    Person *person;
    if(isFiltered)
    {
        person = [self.filteredTableData objectAtIndex:indexPath.row];
    }
    else
    {
       NSArray *filteredContacts = AllFilteredContacts[indexPath.section];
        person = filteredContacts[indexPath.row];
    }
    
    
    ContactDetailViewController *destinationViewController =(ContactDetailViewController *) segue.destinationViewController;
    destinationViewController.person = person;
}

- (NSString *)removeUnnecssaryCharacter:(NSString *)string {
    
    NSString * strippedNumber = [string stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [string length])];
    
    return strippedNumber;
}

-(IBAction) btnDialpadPressed:(id)sender{

    DialpadViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
    [self presentViewController:viewController animated:YES completion:nil];
    
}

- (IBAction)newContactAdd:(id)sender {
    [self createNewPerson:@""];
}


//Create new person
- (void)createNewPerson:(NSString*)number
{
    
    ABRecordRef newPerson = ABPersonCreate();
    CFErrorRef error = NULL;
    
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue,(__bridge CFTypeRef)(number), (CFStringRef)@"home", NULL);
    ABRecordSetValue(newPerson, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    NSAssert( !error, @"Something bad happened here." );
    
    // Create and set-up the new person view controller
    ABNewPersonViewController* newPersonViewController = [[ABNewPersonViewController alloc] initWithNibName:nil bundle:nil];
    [newPersonViewController setDisplayedPerson:newPerson];
    [newPersonViewController setNewPersonViewDelegate:self];
    
    // Wrap in a nav controller and display
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newPersonViewController];
    [self presentModalViewController:navController animated:YES];
    
    CFRelease(newPerson);
}

-(void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(ABRecordRef)person{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self setAddressBook];
}

@end
