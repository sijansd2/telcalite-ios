//
//  CallLogDetailsViewController.h
//  SymlexPro
//
//  Created by USER on 9/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogsHolder.h"
#import "DBManager.h"

@interface CallLogDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UIImageView *coverPhoto;
@property (nonatomic,weak) IBOutlet UIImageView *profilePhoto;
@property (nonatomic,weak) IBOutlet UILabel *name;

@property(nonatomic,strong) LogsHolder *logUser;

@end
