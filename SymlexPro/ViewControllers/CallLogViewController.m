
#import "CallLogViewController.h"
#import "DialpadViewController.h"
#import <AddressBook/AddressBook.h>
#import "CallLogTableViewCell.h"
#import "CallLogDetailsViewController.h"
#import "DBManager.h"
#import "Constans.h"
#import "MyApp.h"
@interface CallLogViewController (){
    
    NSMutableArray *allCallLogs;
    ABAddressBookRef addressBook;
    CFArrayRef all;
    
}

@end

@implementation CallLogViewController

- (void)viewDidLoad {
     [super viewDidLoad];
    allCallLogs = [[NSMutableArray alloc] init];
    CFErrorRef error = nil;
    addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    all = ABAddressBookCopyArrayOfAllPeople(addressBook);
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {});
    
    
    
   
    
    //register an observer for registration callback coming from myapp.m
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processRegState:)
                                                 name: kSIPRegState
                                               object:nil];
    
    [self initStatusButton:@"red_status"];
}


-(void) viewWillAppear:(BOOL)animated{

    [self loadData];
    
}
-(void) viewDidAppear:(BOOL)animated{
    
    get_account_status();
    
}

-(void) initStatusButton :(NSString *) imageName{
    
    UIImage *image = [UIImage imageNamed:imageName];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,15, 15)];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.adjustsImageWhenHighlighted = NO;
    UIBarButtonItem *leftButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}
- (void)processRegState:(NSNotification *)notification
{
    NSDictionary *dictionary = [notification userInfo];
    
    //  NSLog(@"Status in phoneview: %d",[[dictionary objectForKey:@"Status"] intValue]);
    if ([[dictionary objectForKey:@"Status"] intValue] == 200){
        [self initStatusButton:@"green_status"];
    }
    else if ([[dictionary objectForKey:@"StatusText"] isEqual:@"registersent"]){
        [self initStatusButton:@"yellow_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 403){
        [self initStatusButton:@"red_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 404){
        [self initStatusButton:@"red_status"];
    }else if ([[dictionary objectForKey:@"Status"] intValue] == 407){
        [self initStatusButton:@"red_status"];
    }
    
    else{
        [self initStatusButton:@"red_status"];
    }
    
}


-(void) loadData{
    
    allCallLogs = [[NSMutableArray alloc] init];
    
    allCallLogs =[[NSMutableArray alloc] initWithArray:[[DBManager getSharedInstance] getAllCallLogs]];
    NSLog(@"count---%ld",allCallLogs.count);
    [self.callLogTable reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//MARK: TableView Callbacks
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CallLogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    LogsHolder *temp =(LogsHolder*)allCallLogs[indexPath.row];
    
    
    cell.lbCallStatus.text = temp.call_status;
    
    // Convert string to date object 2017-08-12 8:20:33
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setLocale:[NSLocale currentLocale]];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *olddate = [dateFormat dateFromString:temp.date];
    
    [dateFormat setDateFormat:@"hh:mm a"];
    NSString *date = [dateFormat stringFromDate:olddate];
    
    cell.lbTime.text=date;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setCellProfile:cell Number:temp];
        cell.imgProfile.layer.cornerRadius = 20;
        cell.imgProfile.clipsToBounds = YES;
        cell.imgProfile.layer.masksToBounds = YES;
        cell.lbCallStatus.text = [NSString stringWithFormat:@"%@,%@",temp.call_status,temp.Duration];
    });
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return allCallLogs.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [self.callLogTable deselectRowAtIndexPath:indexPath animated:NO];
    
    LogsHolder *log = allCallLogs[indexPath.row];
    
//    DialpadViewController *viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
//    viewcontroller.phoneNumber = log.number;
//    [self presentViewController:viewcontroller animated:YES completion:nil];
    
    DialpadViewController *viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
    dialingNumber =  log.number;
    self.tabBarController.selectedIndex = 1;
}

-(NSString *) getDuration:(NSString *) seconds{

    NSTimeInterval interval = [seconds doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [formatter stringFromDate:date];
}
-(void) setCellProfile:(CallLogTableViewCell *)cell Number:(LogsHolder*)log{
    
    if (log.number == nil)
        return;
    
    
    CFIndex n = ABAddressBookGetPersonCount(addressBook);
    ABRecordRef record=NULL;
    
    
    for( int i = 0 ; i < n ; i++ )
    {
        ABRecordRef ref =CFArrayGetValueAtIndex(all, i);
        
        ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        
        for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
        {
            
            
            NSString *newPhoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, 0));
            
            newPhoneNumber = [self removeUnnecssaryCharacter:newPhoneNumber];
            
            if((newPhoneNumber.length >=7) &&(log.number.length >=7)){
                
                NSString *phoneContactsNumber =[newPhoneNumber substringFromIndex: MAX([newPhoneNumber length] - 7, 0)];
                NSString *dialNumber =[log.number substringFromIndex: MAX([log.number length] - 7, 0)];
                
                if([phoneContactsNumber isEqualToString:dialNumber])
                {
                    record = ref;
                    
                    i=(int)n;
                }else{
                    
                }
            }else{
                
                
                if([newPhoneNumber isEqualToString:log.number])
                {
                    record = ref;
                    
                    i=(int)n;
                }else{
                    
                }
                
            }
        }
        
        
        CFRelease(phones);
    }
    
    //CFRelease(all);
    
    if(record){
        
        NSString *contactname =CFBridgingRelease(ABRecordCopyCompositeName(record));
        cell.lbName.text=contactname;
        log.name = contactname;
        if (ABPersonHasImageData(record)) {
            
            UIImage *img =
            img= [UIImage imageWithData:CFBridgingRelease(ABPersonCopyImageDataWithFormat(record,kABPersonImageFormatThumbnail))];
            cell.imgProfile.image = img;
            log.image =img;
        }else{
            
            [cell.imgProfile setImage:[UIImage imageNamed: @"new_contact"]];
            log.image =[UIImage imageNamed: @"new_contact"];
        }
        
    }else{
        
        [cell.imgProfile setImage:[UIImage imageNamed: @"new_contact"]];
        cell.lbName.text=log.number;
        log.image =[UIImage imageNamed: @"new_contact"];
        log.name = log.number;
    }
    
    //    if(addressBook){
    //
    //        CFRelease(addressBook);
    //    }
}

- (NSString *)removeUnnecssaryCharacter:(NSString *)string {
    
    NSString * strippedNumber = [string stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [string length])];
    
    return strippedNumber;
}

-(IBAction) btnDialpadPressed:(id)sender{
    DialpadViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
    [self presentViewController:viewController animated:YES completion:nil];
}


-(NSIndexPath *) getButtonIndexPath:(UIButton *) button
{
    CGRect buttonFrame = [button convertRect:button.bounds toView:self.callLogTable];
    return [self.callLogTable indexPathForRowAtPoint:buttonFrame.origin];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if([segue.identifier isEqualToString:@"logDetails"]){
        
        NSIndexPath *path = [self getButtonIndexPath:sender];
        LogsHolder *log = allCallLogs[path.row];
        CallLogDetailsViewController *controller = segue.destinationViewController;
        controller.logUser = log;
    }
    
}

@end
