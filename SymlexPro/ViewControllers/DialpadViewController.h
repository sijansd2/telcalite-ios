//
//  DialpadViewController.h
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dialer_keypadView.h"
#import "GetDataController.h"
#import "NetworkManager.h"
#import <AddressBookUI/AddressBookUI.h>

@interface DialpadViewController : UIViewController<KeyPadDelegate,UITextFieldDelegate,GetDataControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lbHeader;
@property (weak, nonatomic) IBOutlet UILabel *lbFooter;

@property (nonatomic,weak) IBOutlet UITextField *tfDialpad;
@property (weak, nonatomic) IBOutlet UILabel *lbRegStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIButton *btnIvr;

@property (strong, nonatomic) NSString *phoneNumber;

- (IBAction)deletePresses:(id)sender;
- (IBAction)keyEvent:(UIButton *)sender;
- (IBAction)btnCancelPressed:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblBalance;
- (IBAction)makeCallPressed:(id)sender;
- (IBAction)addPressed:(id)sender;

@end
