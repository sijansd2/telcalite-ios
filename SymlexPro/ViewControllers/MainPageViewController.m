//
//  MainPageViewController.m
//  ShasthoNet
//
//  Created by Riaz on 6/22/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import "MainPageViewController.h"
#import "AppDelegate.h"
#import "TabViewController.h"
#import "Constans.h"
@interface MainPageViewController ()

@end

@implementation MainPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pageviewcontroller"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    
    ContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    self.pageControl.numberOfPages = 3;
    
    [self.view addSubview: self.pageControl];
    
    [self.view bringSubviewToFront:self.pageControl];
    [self.view bringSubviewToFront:self.btnLogin];
    [self.view bringSubviewToFront:self.btnSignUp];
    
    
            for (UIView *view in self.pageViewController.view.subviews ) {
                if ([view isKindOfClass:[UIScrollView class]]) {
                    UIScrollView *scroll = (UIScrollView *)view;
    
                    scroll.delegate =self;
                    break;
                }
            }
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
   NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    
    if([userDefault objectForKey:data_isAccountCreated] != nil &&
       [[userDefault objectForKey:data_isAccountCreated] isEqualToString:@"true"]){
        
        TabViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
        AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        [app.window setRootViewController:viewController];
        
    }
    
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
 
        return nil;

    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ContentViewController*) viewController).pageIndex;
    
    
    
    if (index ==2 ) {
        
        return nil;
    }
    
    index++;
    
    return [self viewControllerAtIndex:index];
}


- (ContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    
    ContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentviewcontroller"];
    pageContentViewController.pageIndex = index;
    pageContentViewController.view.tag = index;
    
    return pageContentViewController;
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers
{
    //ContentViewController *pageContentView = (ContentViewController*) pendingViewControllers[0];
    
}


- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    if(completed) {
    // At this point, we can safely query the API to ensure
    // that we are fully in sync, just in case.
    self.currentIndex = [self.pageViewController.viewControllers indexOfObject:[pageViewController.viewControllers objectAtIndex:0]];
        
    ContentViewController *pageContentViewController = (ContentViewController *)[self.pageViewController.viewControllers objectAtIndex:0];
    self.pageControl.currentPage = pageContentViewController.view.tag;
    }
    
    
}



-(void) scrollViewDidScroll:(UIScrollView *)scrollView{


    if (self.pageControl.currentPage == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width) {
        
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
        
    } else if (self.pageControl.currentPage == 2
               
               && scrollView.contentOffset.x > scrollView.bounds.size.width) {
        
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
        
    }
}


-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{


    if (self.pageControl.currentPage == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
        
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
        
    } else if (self.pageControl.currentPage == 2
               && scrollView.contentOffset.x >= scrollView.bounds.size.width) {
        
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
        
    }
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}



@end
