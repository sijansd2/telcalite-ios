//
//  WelcomeViewController.m
//  Telca Lite
//
//  Created by Sijan's Mac on 8/5/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import "WelcomeViewController.h"
#import "SignUpViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "Constans.h"

@interface WelcomeViewController (){
    bool isIpv6;
}

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getAppReleaseConfig];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) getAppReleaseConfig{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *url = releaseConfigLink;
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        NSString *aStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        if([aStr containsString:@"\n"]){
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }
        
        if([aStr containsString:@"\r"]){
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        }
        
        NSString * appBuildNum = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        NSLog(@"appBuildNum-------------%@----%@",appBuildNum,aStr);
        if (aStr != nil && [aStr isEqualToString:appBuildNum])
        {
            NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
            [userDef setObject:@"true" forKey:@"isIpv6"];
            isIpv6 = true;
        }else{
            
            NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
            [userDef setObject:@"false" forKey:@"isIpv6"];
            isIpv6 = false;
        }
        
        [self openStoryBoard:isIpv6];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"%@",error);
        
        NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
        [userDef setObject:@"false" forKey:@"isIpv6"];
        
        [self openStoryBoard:false];
    }];
}

-(void)openStoryBoard: (Boolean) isIpv6{
    
    if(isIpv6){
        AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        app.window.rootViewController = [[UIStoryboard storyboardWithName:@"Review" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
    }
    else{
        AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        app.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];

    }
}

@end
