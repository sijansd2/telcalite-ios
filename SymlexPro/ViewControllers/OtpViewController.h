//
//  OtpViewController.h
//  SymlexPro
//
//  Created by USER on 8/19/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataController.h"

@interface OtpViewController : UIViewController<PostDataControllerDelegate>

@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *token;
@property (nonatomic,strong) NSString *userExist;

@property (weak, nonatomic) IBOutlet UIView *otpView;
@property (weak, nonatomic) IBOutlet UIView *wellcomeView;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (nonatomic,weak) IBOutlet UITextField *tfPincode;
@property (nonatomic,weak) IBOutlet UIButton *btnSetPin;
-(IBAction) btnSetPinPressed:(id)sender;
- (IBAction)proceedToLogin:(id)sender;

@end
