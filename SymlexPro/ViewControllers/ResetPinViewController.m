
#import "ResetPinViewController.h"
#import "MyEncryption.h"
#import "Constans.h"
#import "NetworkManager.h"
#import "UIView+Toast.h"

@interface ResetPinViewController (){
    NSUserDefaults* userDefault;
}

@end

@implementation ResetPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    
    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//MARK: Functions

- (IBAction)savePressed:(id)sender {
    
    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        [self changePin];
        [self dismiskeyboard];
        
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    
}

-(void)dismiskeyboard{
    [self.tfCurrentPin resignFirstResponder];
    [self.tfNewPin resignFirstResponder];
    [self.tfConfirmPin resignFirstResponder];
}

-(void)changePin{
    NSString* user = [userDefault stringForKey:data_user_name];
    NSString* pass = [userDefault stringForKey:data_password];
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,resetPin];
    NSString* md5Str = [[NSString stringWithFormat: @"%@%@",user,pass] MD5String];
    NSDictionary *param = @{@"username": user,
                            @"password": pass,
                            @"api_key": apiKey,
                            @"sec_key": md5Str,
                            @"old_pin": self.tfCurrentPin.text,
                            @"new_pin": self.tfNewPin.text};
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    [controller initWithURL:url withData:param];
}


-(void)forgotPin{
    
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,forgetPassword];
    NSDictionary *param = @{@"email": @"sijan@gmail.com",
                            @"api_key": @"12985c70637f5775530bf704cdfa7b4a"};
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    [controller initWithURL:url withData:param];
}



//MARK: Http post callbacks
-(void)postFinishedWithData:(id)responseObject{
    
    NSString *msg = [responseObject objectForKey:@"message"];
    [self showAlert:@"Server Response" messageBody:msg];
}

-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    NSLog(@"Server response : postFinishedWithData");
}


-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"%@",error);
    [self showAlert:@"Error" messageBody:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
}


//MARK: Alert
-(void) showAlert:(NSString*)title messageBody:(NSString*)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Server Response" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    //NSLog(@"you pressed Yes button");
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


@end
