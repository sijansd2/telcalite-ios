//
//  WhiteBorderedTextField.h
//  SymlexPro
//
//  Created by USER on 8/10/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface WhiteBorderedTextField : UITextField

@property(nonatomic, strong) IBInspectable UIColor *text_color;
@property(nonatomic, strong) IBInspectable UIColor *borderColor;

@property(nonatomic, strong) IBInspectable UIColor *backgroundNormalColor;

@end
