//
//  CancelButton.m
//  helloworld
//
//  Created by Riaz on 5/31/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import "WhiteBorderedButton.h"
#import "ColorUtility.h"
@implementation WhiteBorderedButton

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addAction];
    }
    return self;
}

- (void)addAction {
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpOutside];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchCancel];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventPrimaryActionTriggered];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.adjustsImageWhenHighlighted = NO;
    
    self.layer.borderWidth = 1;
    
    if(self.backgroundNormalColor){
        self.backgroundColor = self.backgroundNormalColor;
    }else{
        self.backgroundColor = [UIColor clearColor];
    }
    
    if(self.borderColor){
    
        self.layer.borderColor = self.borderColor.CGColor;
    }else{
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    if(self.customTextColor){
    
        self.titleLabel.textColor = self.customTextColor;
    }else{
        self.titleLabel.textColor = [ColorUtility colorWithHexString:textColor];
    }
    
    self.layer.cornerRadius =5;
    
}

-(void) addBackgroundColor :(NSString *)firstColorCode{
    
    self.backgroundColor = [ColorUtility colorWithHexString:firstColorCode];
    
}

- (void)pressed:(UIButton *)btn {
    
    //[btn setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]];
    
}

- (void)touchUp:(UIButton *)btn {
    
   // [btn setBackgroundColor:[UIColor whiteColor]];
    
}

@end
