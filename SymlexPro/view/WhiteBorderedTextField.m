//
//  WhiteBorderedTextField.m
//  SymlexPro
//
//  Created by USER on 8/10/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "WhiteBorderedTextField.h"
#import "ColorUtility.h"

@implementation WhiteBorderedTextField

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.borderWidth = 1;
    
    if(self.text_color){
        self.textColor = self.text_color;
    }else{
        self.textColor = [UIColor whiteColor];
    }
    
    if(self.backgroundNormalColor){
        self.backgroundColor = self.backgroundNormalColor;
    }else{
        self.backgroundColor = [UIColor clearColor];
    }
    
    if(self.borderColor){
        
        self.layer.borderColor = self.borderColor.CGColor;
    }else{
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    }

    
    self.layer.cornerRadius =0;
    
}


@end
