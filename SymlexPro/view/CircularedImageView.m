//
//  CircularedImageView.m
//  SymlexPro
//
//  Created by USER on 8/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "CircularedImageView.h"

@implementation CircularedImageView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    return self;
}

- (instancetype)init {
    self = [super init];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.layer.frame.size.width/2;
    self.layer.borderWidth=10;
    self.layer.borderColor=[UIColor whiteColor].CGColor;
}

@end
