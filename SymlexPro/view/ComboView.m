//
//  CityChooserViewController.m
//  helloworld
//
//  Created by Riaz on 5/18/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import "ComboView.h"

@interface ComboView ()

@end

@implementation ComboView{
    
    NSMutableArray *List;
    NSMutableArray *filteredTableData;
    
}
@synthesize isFiltered,searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    searchBar.delegate=self;
    searchBar.enablesReturnKeyAutomatically=NO;
    
    List = [[NSMutableArray alloc] init];
    filteredTableData = [[NSMutableArray alloc] init];
    
    self.titleBar.text = [NSString stringWithFormat:@"Choose %@",self.comboType];
    [List addObjectsFromArray:self.comboArray];
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSDictionary *item;
    
    if(isFiltered)
    {
        item = [filteredTableData objectAtIndex:indexPath.row];
    }
    else
    {
        item = [List objectAtIndex:indexPath.row];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(userFinishedChoosingitem: andType:)]) {
        [self.delegate userFinishedChoosingitem:item andType:self.comboType];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(self.isFiltered){
        return filteredTableData.count;
    }else{
        return List.count;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *item;
    if(isFiltered){
        item = filteredTableData[indexPath.row];
    }else{
        item = List[indexPath.row];
    }
    
    NSString *name = item[self.comboNameProperty];
    cell.textLabel.text =name;
    
    NSString *filename = [name stringByAppendingString:@".png"];
    filename = [filename stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    filename = [filename stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
    [cell.imageView setImage:[UIImage imageNamed:filename]];
    
    return cell;
}


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    
    }else
    {
        isFiltered = true;
        
        [filteredTableData removeAllObjects];
        
        for(NSDictionary *item in List)
        {
            NSRange range = [item[self.comboNameProperty] rangeOfString:text options:NSCaseInsensitiveSearch];
            if(range.location != NSNotFound){
                [filteredTableData addObject:item];
            }
        }
    }
    
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchbar
{
    [searchbar resignFirstResponder];
    // Do the search...
}


- (IBAction)btnClosePressed:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeButtonClicked)]) {
        [self.delegate closeButtonClicked];
    }
}
@end
