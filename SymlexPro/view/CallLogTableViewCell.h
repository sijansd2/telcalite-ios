//
//  CallLogTableViewCell.h
//  SymlexPro
//
//  Created by USER on 9/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallLogTableViewCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UILabel *lbName;
@property(weak,nonatomic) IBOutlet UILabel *lbTime;
@property(weak,nonatomic) IBOutlet UILabel *lbCallStatus;
@property(weak,nonatomic) IBOutlet UIImageView *imgProfile;
@property(weak,nonatomic) IBOutlet UIImageView *imgCall;

@end
