//
//  CorneredView.m
//  SymlexPro
//
//  Created by USER on 8/16/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "CorneredView.h"

@implementation CorneredView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
   
    return self;
}

- (instancetype)init {
    self = [super init];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
   
    self.layer.cornerRadius = 10;
    
}

@end
