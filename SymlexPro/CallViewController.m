//
//  CallViewController.m
//  SymlexPro
//
//  Created by admin on 9/19/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "CallViewController.h"
#import "Constans.h"
#import <AddressBook/AddressBook.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import "PulsingHaloLayer.h"
#import "LogsHolder.h"
#import "DBManager.h"
#import "SVProgressHUD.h"
#import "NSNotificationAdditions.h"

@interface CallViewController (){
    
    pj_status_t status;
    pjsua_call_id call_id;
    pjsua_call_setting call_opt;
    NSString *callDuration;
    NSTimer *timer;
}

@property (nonatomic, weak) PulsingHaloLayer *halo;

@end

@implementation CallViewController

@synthesize phoneNumber,callType;

NSString *callIdStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    //NSString *isShowAds = [userDefault stringForKey:data_showAds];
//    if([isShowAds isEqualToString:@"1"]){
//        _adview.adUnitID = @"ca-app-pub-3321153666811618/7097669082";
//        _adview.rootViewController = self;
//        [_adview loadRequest:[GADRequest request]];
//    }
   
    
    callDuration = @"";
    [self setNeedsStatusBarAppearanceUpdate];
    if(callType == callTypeDialed){
        
        [self setOutGoingView];
         NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        if([[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
            
            [self sendPush];
        }
        
        [NSTimer scheduledTimerWithTimeInterval:.2f target:self selector:@selector(initCall) userInfo:nil repeats:NO];
        
    }
    else if(callType == callTypeFree){
        
        [self setOutGoingView];
        
        [self sendPush];
       
        [NSTimer scheduledTimerWithTimeInterval:.2f target:self selector:@selector(initCall) userInfo:nil repeats:NO];
    }
    
    else if(callType == callTypeIncoming){
        
        [self setIncomingView];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processCallState:)
                                                 name: kSIPCallState
                                               object:nil];
    
}


-(void) sendPush{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *user = [userDefault stringForKey:data_user_name];
    NSString *url_string = [send_push_link stringByReplacingOccurrencesOfString:@"##userid##" withString:phoneNumber];
    url_string = [url_string stringByReplacingOccurrencesOfString:@"##sender##" withString:user];
    [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    
    phoneNumber = [NSString stringWithFormat:@"%@",phoneNumber];
    
}


-(void) initWithCallId:(pjsua_call_id )callid{
    
    call_id=callid;
    
    [self insertCallLog];
}

-(void) initWithCallId:(pjsua_call_id)callid withString:(NSString *) callIDStrx{

    call_id=callid;
    callIdStr = callIDStrx;
    [self insertCallLog];

}

-(void)viewDidAppear:(BOOL)animated{
    
    [self SetUpView];
    [self setAcceptBtnAnimation];
    [self setDragDrop];
}

-(void) initCall{
    
    
    NSUserDefaults *theUser = [NSUserDefaults standardUserDefaults];
    
    NSString *server = [theUser stringForKey:data_sip_ip];
    
    NSString *number;
    number = [NSString stringWithFormat:@"sip:%@@%@",phoneNumber,server];
    
    if([[theUser objectForKey:@"isIpv6"] isEqualToString:@"true"]){
        number = [NSString stringWithFormat:@"sip:%@@%@",phoneNumber,server];
    }
    char *phn_number =(char *)[number UTF8String];
    
    pjsua_call_setting_default(&call_opt);
    call_opt.aud_cnt=1;
    call_opt.vid_cnt=0;
    
    
    status=makeCall(phn_number,&call_id,call_opt);
    
    if(status==PJSIP_EINVALIDURI){
        
        //lbCallStatus.text = @"Invalid Phone Number";
        
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
        
    }
    
    NSLog(@"call status------%d",(int)status);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)endPressed:(id)sender {
    
    endCall();
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)speakerPressed:(id)sender {
    [self setAudioOutputSpeaker:![sender isSelected]];
    [sender setSelected: ![sender isSelected]];
}

- (IBAction)holdPressed:(id)sender {
    [self setHold:![sender isSelected]];
    [sender setSelected: ![sender isSelected]];
}

- (IBAction)mutePressed:(id)sender {
    [self setMute:![sender isSelected]];
    [sender setSelected: ![sender isSelected]];
}



//MARK: Functions
pjsua_call_setting call_opt;
-(void)setHold:(BOOL)enable{
    
    if(enable){
        pjsua_call_set_hold(call_id, PJSUA_CALL_HOLD_TYPE_DEFAULT);
    }
    else{
        
        
        pjsua_call_setting_default(&call_opt);
        call_opt.flag = 1;
        pjsua_call_reinvite2(call_id, &call_opt, NULL);
        //unholdCall(call_id,call_opt);
    }
}

- (void)setAudioOutputSpeaker:(BOOL)enabled
{
    
    AVAudioSession *session =   [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setMode:AVAudioSessionModeVoiceChat error:&error];
    if (enabled) // Enable speaker
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    else // Disable speaker
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
    [session setActive:YES error:&error];
}


- (void)setMute:(BOOL)enable
{
    if (enable)
        pjsua_conf_adjust_rx_level(0,0.0f);
    else
        pjsua_conf_adjust_rx_level(0,1.0f);
}


-(void) SetUpView{
    
    
    float height = self.calleeImage.layer.frame.size.height;
    self.calleeImage.layer.cornerRadius = height/2;
    self.calleeImage.layer.borderWidth = 2;
    self.calleeImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.calleeImage.clipsToBounds = YES;
    self.calleeImage.layer.masksToBounds = YES;

    //lvCalleNumber.text =phoneNumber;
    
    if(callType==callTypeDialed){
        
        //Receive view hide
        
    }else if (callType==callTypeIncoming){
        
        //Receive view show
        
    }

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if(![[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
      [self setCellProfile:phoneNumber];
    }

    PulsingHaloLayer *layer = [PulsingHaloLayer layer];
    self.halo = layer;
    [self.btnAccept.superview.layer insertSublayer:self.halo below:self.btnAccept.layer];
    self.halo.position = self.btnAccept.center;
    UIColor *color = [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0];
    [self.halo setBackgroundColor:color.CGColor];
    self.halo.radius = 150;
    self.halo.haloLayerNumber = 6;
    [self.halo start];
}

-(void) setCellProfile:(NSString*)phnNumber{
    
    if (phnNumber == nil)
        return;
    
    CFErrorRef error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {});
    
    
    CFArrayRef all = ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    
    CFIndex n = ABAddressBookGetPersonCount(addressBook);
    ABRecordRef record=NULL;
    
    
    for( int i = 0 ; i < n ; i++ )
    {
        ABRecordRef ref = CFArrayGetValueAtIndex(all, i);
        
        ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        
        for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
        {
            
            //  CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
            
            NSString *newPhoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, 0));
            
            newPhoneNumber = [self removeUnnecssaryCharacter:newPhoneNumber];
            
            if((newPhoneNumber.length >=7) &&(phnNumber.length >=7)){
                
                NSString *phoneContactsNumber =[newPhoneNumber substringFromIndex: MAX([newPhoneNumber length] - 7, 0)];
                NSString *dialNumber =[phnNumber substringFromIndex: MAX([phnNumber length] - 7, 0)];
                
                
                
                if([phoneContactsNumber isEqualToString:dialNumber])
                {
                    record = ref;
                    
                    i=(int)n;
                }else{
                    
                }
            }else{
                
                
                if([newPhoneNumber isEqualToString:phnNumber])
                {
                    record = ref;
                    
                    i=(int)n;
                }else{
                    
                    
                }
                
            }
        }
        
        
        CFRelease(phones);
    }
    
    CFRelease(all);
    
    if(record){
        
        NSString *contactname =CFBridgingRelease(ABRecordCopyCompositeName(record));
        self.name.text=contactname;
        
        // Check for contact picture
        if (ABPersonHasImageData(record)) {
            
            if ( &ABPersonCopyImageDataWithFormat != nil ) {
                // iOS >= 4.1
                self.calleeImage.image= [UIImage imageWithData:CFBridgingRelease(ABPersonCopyImageDataWithFormat(record,kABPersonImageFormatThumbnail))];
            } else {
                // iOS < 4.1
//                self.calleeImage.image= [UIImage imageWithData:CFBridgingRelease(ABPersonCopyImageData(record))];
            }
        }
        
    }else{
        self.name.text=phnNumber;
    }
    
    if(addressBook){
        
        CFRelease(addressBook);
    }
}


- (NSString *)removeUnnecssaryCharacter:(NSString *)string {
    
    NSString * strippedNumber = [string stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [string length])];
    
    return strippedNumber;
}



- (void)processCallState:(NSNotification *)notification
{
    
    //pjsua_acc_info info;
    //pjsua_acc_id acc_id;
    //NSString *status;
    //acc_id = [[[ notification userInfo ] objectForKey: @"AccountID"] intValue];
    NSDictionary *dictionary = [notification userInfo];
    
    self.duration.text=[dictionary objectForKey:@"StatusText"];
    
    
    int state = [[[ notification userInfo ] objectForKey: @"State"] intValue];
    int lastState = [[[ notification userInfo ] objectForKey: @"LastStatus"] intValue];
    NSLog(@"code--in callview--%d",lastState);
    call_id =[[dictionary objectForKey:@"CallID"] intValue];
    callIdStr = [dictionary objectForKey:@"callIdStr"];
    
    
    //NSLog(@"Call id str ==%@",callIdStr);
    
    if(lastState == 408){
    
        [self initCall];
        return;
    }
    
    switch(state)
    {
        case PJSIP_INV_STATE_NULL: // Before INVITE is sent or received.
            
            self.duration.text=@"Calling...";
            
            break;
        case PJSIP_INV_STATE_CALLING: // After INVITE is sent.
            
            self.duration.text=@"Calling...";
            [UIDevice currentDevice].proximityMonitoringEnabled = YES;
            break;
        case PJSIP_INV_STATE_INCOMING: // After INVITE is received.
            self.duration.text=@"Incoming Call...";
            
            break;
        case PJSIP_INV_STATE_EARLY: // After response with To tag.
            self.duration.text=@"Calling...";
            [self insertCallLog];
            break;
            
        case PJSIP_INV_STATE_CONNECTING: // After 2xx is sent/received.
            self.duration.text=@"Ringing...";
            break;
            
        case 408:
            [self initCall];
            break;
        case PJSIP_INV_STATE_CONFIRMED: // After ACK is sent/received.
            
            //viewAcceptReject.hidden=TRUE;
            //viewEndCall.hidden=FALSE;
            self.duration.text=@"Call Connected";
            timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                     target:self
                                                   selector:@selector(timeout:)
                                                   userInfo:nil
                                                    repeats:YES];
            [timer fire];
            break;
        case PJSIP_INV_STATE_DISCONNECTED:
            [UIDevice currentDevice].proximityMonitoringEnabled = NO;
            self.duration.text=@"Call Disconnected...Please Wait";
            [self setAudioOutputSpeaker:false];
            [self setMute:false];
            [self upDateCallLog];
            
            
            if (timer)
            {
                [timer invalidate];
                timer = nil;
            }
            
            
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //code to be executed on the main queue after delay
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            
//        NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
//                                  @"aaa",@"ssss",
//                                  nil];
//
//        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kBalanceState
//                                                                            object:nil
//                                                                          userInfo:userinfo];
            
            break;
    }
    
}


- (void)timeout:(id)unused
{
    pjsua_call_info ci;
    
    pjsua_call_get_info(call_id, &ci);
    
    if (ci.connect_duration.sec >= 3600)
    {
        long sec = ci.connect_duration.sec % 3600;
        self.duration.text=[NSString stringWithFormat:@"%ld:%02ld:%02ld",
                             ci.connect_duration.sec / 3600,
                             sec/60, sec%60];
        
        callDuration =[NSString stringWithFormat:@"%ld:%02ld:%02ld",
                       ci.connect_duration.sec / 3600,
                       sec/60, sec%60];
    }
    else
    {
        self.duration.text=[NSString stringWithFormat:@"%02ld:%02ld",
                             (ci.connect_duration.sec)/60,
                             (ci.connect_duration.sec)%60];
        
        callDuration =[NSString stringWithFormat:@"%02ld:%02ld",
                       (ci.connect_duration.sec)/60,
                       (ci.connect_duration.sec)%60];

    }
}


-(void)setAcceptBtnAnimation{
    
    [self.btnAccept setUserInteractionEnabled:YES];
    int height = self.acceptContainer.layer.frame.size.height/2;
    int width = self.acceptContainer.layer.frame.size.width/2;
    self.btnAccept.center = CGPointMake(width, height);    
    
    CAKeyframeAnimation *animation;
    
    animation = [CAKeyframeAnimation animationWithKeyPath:@"position.x"];
    animation.duration = 0.3f;
    animation.repeatCount = 100;
    animation.values = [NSArray arrayWithObjects:
                        [NSNumber numberWithFloat:width],
                        [NSNumber numberWithFloat:width+20],
                        [NSNumber numberWithFloat:width],
                        [NSNumber numberWithFloat:width-20],
                        [NSNumber numberWithFloat:width], nil];
    
    animation.keyTimes = [NSArray arrayWithObjects:
                          [NSNumber numberWithFloat:0.0],
                          [NSNumber numberWithFloat:0.25],
                          [NSNumber numberWithFloat:.5],
                          [NSNumber numberWithFloat:0.75],
                          [NSNumber numberWithFloat:1.0], nil];
    
    animation.removedOnCompletion = NO;
    
    
    [self.btnAccept.layer addAnimation:animation forKey:nil];
}


-(void)setDragDrop{
    
    [self.btnAccept setTag:100];
    [self.btnAccept setUserInteractionEnabled:YES];
    
    self.tempIV = [[UIImageView alloc] init];
    [self.tempIV setFrame:CGRectMake(0, 300, 80, 80)];
    [self.tempIV setTag:102];
    [self.tempIV setUserInteractionEnabled:YES];
    [self.acceptContainer addSubview:self.tempIV];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    
    if([[touch view] tag] == 100)
    {
        [self.tempIV setImage:self.btnAccept.image];
        [self.tempIV setCenter:[touch locationInView:self.acceptContainer]];
        [self.btnAccept setImage:[UIImage imageNamed:@""]];
        
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if([[touch view] tag] == 100){
        [self setDragDropMovingView: touch];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if([[touch view] tag] == 100){
    
        [self.tempIV setImage:nil];
        [self.btnAccept setImage:[UIImage imageNamed:@"btn_normal.png"]];
        [self setDragDrop];
        [self setAcceptBtnAnimation];
        
        
        int center = self.acceptContainer.layer.frame.size.width/2;
        
        [self.tempIV setCenter:[touch locationInView:self.acceptContainer]];
        CGPoint point = [touch locationInView:self.acceptContainer];
        
        
        int objectHorizontalPosition = point.x;
        
        if(objectHorizontalPosition > center+10){
            acceptcall(&call_id);
            [self setAudioOutputSpeaker:false];
            [self setOutGoingView];
        }
        else if(objectHorizontalPosition < center-10){
            [self setAudioOutputSpeaker:false];
            endCall();
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            //[self.tempIV setImage:[UIImage imageNamed:@"btn_normal.png"]];
        }
    }
    
    
    
}

-(void) viewWillDisappear:(BOOL)animated{

    [self setAudioOutputSpeaker:false];
    [self setMute:false];
    
    NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"aaa",@"ssss",
                              nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kBalanceState
                                                                        object:nil
                                                                      userInfo:userinfo];
}

-(void)setDragDropMovingView:(UITouch*) touch{
    
    int height = self.acceptContainer.layer.frame.size.height;
    int width = self.acceptContainer.layer.frame.size.width;
    int center = width/2;
    
        [self.tempIV setCenter:[touch locationInView:self.acceptContainer]];
        CGPoint point = [touch locationInView:self.acceptContainer];
    
    if(point.x > height){
        
    }
        int objectHorizontalPosition = point.x;
        
        if(objectHorizontalPosition > center+10){
            [self.tempIV setImage:[UIImage imageNamed:@"btn_accept.png"]];
        }
        else if(objectHorizontalPosition < center-10){
            [self.tempIV setImage:[UIImage imageNamed:@"btn_reject.png"]];
        }
        else{
            [self.tempIV setImage:[UIImage imageNamed:@"btn_normal.png"]];
        }
    //}
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setIncomingView{
    
    self.acceptContainer.hidden = NO;
    self.endCallView.hidden = YES;
    self.duration.text=@"Incoming Call..";
}

-(void)setOutGoingView{
    
    self.acceptContainer.hidden = YES;
    self.endCallView.hidden = NO;
    
}


-(void) insertCallLog{
    
    BOOL success;
    if(callIdStr !=NULL){
        
        LogsHolder *log =[[LogsHolder alloc] initWithNumber:phoneNumber Duration:@"00:00:00" CallID:callIdStr Date:[self getCurrentTime] Call_status:callType];
        
        success = [[DBManager getSharedInstance]saveData:log];
        
        if(success)
            NSLog(@"insert success");
    }
    
}


-(void) upDateCallLog{
    
    BOOL success;
    NSString *durations = self.duration.text;
    if ([durations isEqual:@""]) {
        
        durations = @"00:00:00";
    }
    if(callIdStr !=NULL){
        
        LogsHolder *log =[[LogsHolder alloc] initWithNumber:phoneNumber Duration:callDuration CallID:callIdStr Date:[self getCurrentTime] Call_status:callType];
        
        success = [[DBManager getSharedInstance] updateRow:log];
        
        if(success)
            NSLog(@"Update success");
    }
    

}


-(NSString *) getCurrentTime{
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    return [dateFormatter stringFromDate:[NSDate date]];
    
}

@end
