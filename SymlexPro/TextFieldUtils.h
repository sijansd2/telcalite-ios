//
//  TextFieldUtils.h
//  SymlexPro
//
//  Created by USER on 8/13/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface TextFieldUtils : NSObject
+(void) addImageLeftSideTextView:(UITextField *)textField imageName:(NSString *)image;
+(void) addImageRightSideTextView:(UITextField *)textField imageName:(NSString *)image;
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;

@end
