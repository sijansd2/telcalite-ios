//
//  NetworkManager.h
//  SymlexPro
//
//  Created by admin on 9/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+(NetworkManager*)getInstance;
+(Boolean)getNetworkStatus;

@end

