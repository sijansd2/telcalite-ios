//
//  Constans.m
//  SymlexPro
//
//  Created by admin on 7/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "Constans.h"

NSString * const baseUrl = @"http://69.30.193.83/";

NSString * const apiKey = @"12985c70637f5775530bf704cdfa7b4a";

//API Urls
NSString * const voucherRecharge = @"index.php/service/pin_recharge";
NSString * const balance = @"index.php/service/get_balance/";
NSString * const balanceTransfer = @"index.php/service/transfer";
NSString * const changePass = @"index.php/service/change_password";
NSString * const forgetPassword = @"index.php/service/forgot_password";
NSString * const resetPin = @"index.php/service/reset_pin";
NSString * const checkUser = @"index.php/service/get_balance";
NSString * const checkPin = @"index.php/service/check_pin";
NSString * const checkMailVerification = @"index.php/service/check_verified";
//NSString * const configURL = @"http://199.33.126.83/ad_sha/ad_sx.php?as=#opcode#";
//NSString * const configURL = @"http://199.33.126.83/ad_p/ad_pEx.php?ap=#opcode#";
NSString * const configURL = @"http://199.33.126.83/ad_p/ad_ip_pEx.php?ap=22224";
NSString * const configTestURL = @"http://199.33.126.83/ad_p/ad_ip_pEx.php?ap=8989";
NSString * const signUp = @"index.php/service/create_user";
NSString * const verifyOtp = @"index.php/service/verify_otp";
NSString * const check_user = @"index.php/service/check_sipuser";
NSString * const setPin = @"index.php/service/pin_set";
NSString * const data_opcode = @"5464";

//Registration data
NSString * const kSIPCallState = @"CallState";
NSString * const kSIPINCOMINGCall = @"IncomingCall";
NSString * const kSIPRegState  = @"RegState";
NSString * const kBalanceState  = @"BalanceState";
NSString * const KUpdateCallLog= @"updateCallLog";
NSString * const NetworkUpdate=@"NetworkUpdate";


//Zemsettings data
NSString * const data_header = @"header";
NSString * const data_footer = @"footer";
NSString * const data_balancelink = @"balancelink";
NSString * const data_sip_ip = @"sipserver";
NSString * const data_proxy_ip = @"proxyserver";
NSString * const data_turn_ip = @"turnIP";
NSString * const data_encryption_key = @"encryptionkey";
NSString * const data_dp_pad = @"dp_dap";
NSString * const data_pd_pad = @"pd_pad";
NSString * const data_ptime = @"ptime";
NSString * const data_transport = @"transport";
NSString * const data_kaInterval = @"katinterval";
NSString * const data_user_name = @"username";
NSString * const data_password = @"password";
NSString * const data_caller_id = @"callerid";
NSString * const data_registration_timeout = @"regtimeout";
NSString * const data_ivr = @"ivr";
NSString * const data_apikey = @"morapikey";
NSString * const data_smsGateway = @"smsgateway";
NSString * const data_country_code = @"counrtry_code";
NSString * const data_country_id = @"country_id";
NSString * const data_current_balance = @"current_balance";
NSString * const data_is_linphone_acc = @"is_linphone_acc";
NSString * const data_last_dialed_number = @"last_dialed_number";
NSString * const data_device_token = @"deviceToken";
NSString * const data_dialed_number = @"dialedNumber";
NSString * const data_isAccountCreated = @"accountCreated";
NSString * const data_useTurn = @"useturn";
NSString * const data_proxyPort = @"proxyPort";
NSString * const data_showAds = @"showAds";

NSString * dialingNumber= @"";
NSString * const callTypeDialed= @"dialed";
NSString * const callTypeIncoming= @"incoming";
NSString * const callTypeFree= @"freeCall";

//Get release config
NSString* const releaseConfigLink = @"http://conf.aussdjf.xyz/iosapi/telcalite/ios_api.html";

//push notification server link
NSString* const insert_token_link = @"http://sip2.kolpolok.com/telcalite/insert_or_update.php?userid=##userid##&token=##token##";
NSString* const send_push_link = @"http://sip2.kolpolok.com/telcalite/send_push_noti.php?userid=##userid##&senderid=##sender##";
NSString* const decline_push = @"http://sip2.kolpolok.com/telcalite/decline_push_noti.php?userid=##userid##&senderid=##sender##";

@implementation Constans

@end
