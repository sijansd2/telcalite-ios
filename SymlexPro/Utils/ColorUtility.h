//
//  ColorUtility.h
//  SymlexPro
//
//  Created by USER on 8/10/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


//colors
extern NSString * const textColor;
extern NSString * const colorPrimary;

@interface ColorUtility : NSObject

+ (UIColor *)colorWithHexString:(NSString *)hexString;



@end
