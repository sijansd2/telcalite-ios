//
//  Country.h
//  SymlexPro
//
//  Created by USER on 8/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Country : NSObject
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString *code;
@end

