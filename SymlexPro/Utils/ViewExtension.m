//
//  Common.m
//  ShasthoNet
//
//  Created by Riaz on 7/25/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import "ViewExtension.h"

@implementation UIViewController (ViewExtension)

-(void) setBackGroundImage:(NSString *) imageName{

    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:imageName] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}



@end
