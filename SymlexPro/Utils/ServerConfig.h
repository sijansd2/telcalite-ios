//
//  ServerConfig.h
//  SymlexPro
//
//  Created by admin on 8/2/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ServerCongigDelegate;


@interface ServerConfig : NSObject<NSXMLParserDelegate>

@property (nonatomic, assign) id<ServerCongigDelegate> srvDelegate;
@property (strong) NSMutableString *currentAttributeS;
@property (strong) NSXMLParser *parserS;

-(void)fetchedData:(NSData *)responseData;

@end

@protocol ServerCongigDelegate <NSObject>

-(void)successfullyFetchedData;

@end
