//
//  XMLParser.m
//  SymlexPro
//
//  Created by USER on 9/14/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "XMLParser.h"
#import "Constans.h"

@implementation XMLParser

NSString *sip_ip;
NSString *proxy_ip;
NSString *sip_port;
NSString *header;
NSString *footer;
NSString *enc_key;
NSString *transport;
NSString *dp_pad;
NSString *pd_pad;
NSString *ptime;
NSString *balancelink;
NSString *keepalive;
NSString *turn_ip;
NSString *ivr;
NSString *proxyPort;
NSString *showAds;

@synthesize parserS,currentAttributeS;

-(void) parseData: (NSString *)msg
{
    parserS = [[NSXMLParser alloc] initWithData:[msg dataUsingEncoding:NSUTF8StringEncoding]];
    [parserS setDelegate:self];
    [parserS parse];
    
    
}


-(void) parser:(NSXMLParser *) parserS   didStartElement:(NSString *) elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if (![elementName compare:@"dialer"])
    {
    }
    
    
    else  if (![elementName compare:@"hhh"])
    {
        currentAttributeS = [NSMutableString string];
        
    }
    
    else  if (![elementName compare:@"fff"])
    {
        currentAttributeS = [NSMutableString string];
        
    }
    //----------- end --------------------------------------------
    
    else  if (![elementName compare:@"rrr"])
    {
        currentAttributeS = [NSMutableString string];
    }
    else if (![elementName compare:@"ppp"])
    {
        currentAttributeS = [NSMutableString string];
    }
    
    else  if (![elementName compare:@"eee"])
    {
        currentAttributeS = [NSMutableString string];
    }
    
    else  if (![elementName compare:@"tst"])
    {
        currentAttributeS = [NSMutableString string];
    }
    
    else  if (![elementName compare:@"ttt"])
    {
        currentAttributeS = [NSMutableString string];
    }
    else  if (![elementName compare:@"bbb"])
    {
        currentAttributeS = [NSMutableString string];
        // NSLog(@"Ballance : %@",currentAttributeS);
    }
    
    else  if (![elementName compare:@"ddp"])
    {
        currentAttributeS = [NSMutableString string];
    }
    else  if (![elementName compare:@"ppd"])
    {
        currentAttributeS = [NSMutableString string];
    }
    else  if (![elementName compare:@"kat"])
    {
        currentAttributeS = [NSMutableString string];
        
    }
    else  if (![elementName compare:@"ivv"])
    {
        currentAttributeS = [NSMutableString string];
    }
    else  if (![elementName compare:@"tPort"])
    {
        currentAttributeS = [NSMutableString string];
    }
    else  if (![elementName compare:@"tIP"])
    {
        currentAttributeS = [NSMutableString string];
    }
    else  if (![elementName compare:@"show_add"])
    {
        currentAttributeS = [NSMutableString string];
    }
    
    else {
        //nothing
    }
    
    
    
}


//delegate method for when encounter end of element tag

-(void)parser:(NSXMLParser *)parserS didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *) qName

{
    
    
    if (![elementName compare:@"dialer"])
    {
        //[XMLArray addObject:xmlObj];
    }
    else if (![elementName compare:@"rrr"])
    {
        sip_ip = currentAttributeS;
        
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:sip_ip forKey:data_sip_ip];
        
        NSLog(@"Sip_ip---%@",sip_ip);
    }
    else if (![elementName compare:@"ppp"])
    {
        proxy_ip =currentAttributeS;
        
        if([proxy_ip isEqualToString:@"0"])
        {
            proxy_ip=@"";
        }
        
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:proxy_ip forKey:data_proxy_ip];
    }
    else if (![elementName compare:@"hhh"])
    {
        header = currentAttributeS ;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:header forKey:data_header];
    }
    
    else if (![elementName compare:@"fff"])
    {
        footer = currentAttributeS ;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:footer forKey:data_footer];
    }
    
    else if (![elementName compare:@"eee"])
    {
        enc_key = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:enc_key forKey:data_encryption_key];
    }
    
    else if (![elementName compare:@"tst"])
    {
        transport = currentAttributeS;
        if([transport isEqualToString:@"1"])
        {
            transport=@"udp";
        }
        else if ([transport isEqualToString:@"2"])
        {
            transport=@"tcp";
        }
        else if ([transport isEqualToString:@"3"])
        {
            transport=@"tls";
        }
        
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:transport forKey:data_transport];
    }
    
    
    else if (![elementName compare:@"ttt"])
    {
        ptime = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:ptime forKey:data_ptime];
    }
    else if (![elementName compare:@"bbb"])
    {
        balancelink = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:balancelink forKey:data_balancelink];
    }
    
    else  if (![elementName compare:@"ddp"])
    {
        dp_pad = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:dp_pad forKey:data_dp_pad];
    }
    else  if (![elementName compare:@"ppd"])
    {
        pd_pad = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:pd_pad forKey:data_pd_pad];
    }
    else  if (![elementName compare:@"kat"])
    {
        keepalive = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:keepalive forKey:data_kaInterval];
        
    }
    else  if (![elementName compare:@"ivv"])
    {
        ivr = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:ivr forKey:data_ivr];
        
    }
    else  if (![elementName compare:@"tPort"])
    {
        proxyPort = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:proxyPort forKey:data_proxyPort];
        
    }
    else  if (![elementName compare:@"tIP"])
    {
        turn_ip = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:turn_ip forKey:data_turn_ip];
    }
    else  if (![elementName compare:@"show_add"])
    {
        showAds = currentAttributeS;
        NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
        [newUser setObject:showAds forKey:data_showAds];
    }
    
    
}
//delegate method for when encounter attribute of element tag

-(void) parser:(NSXMLParser *)parserS foundCharacters :(NSString *) string
{
    
    if (self.currentAttributeS)
    {
        [self.currentAttributeS appendString:string];
    }
    
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
    
    //NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];
    
    //[newUser removeObjectForKey:data_sip_ip];
    //    [newUser removeObjectForKey:data_turn_ip];
    //    [newUser removeObjectForKey:data_proxy_ip];
    //    [newUser removeObjectForKey:data_encryption_key];
    //    [newUser removeObjectForKey:data_ptime];
    //    [newUser removeObjectForKey:data_transport];
    //    [newUser removeObjectForKey:data_kaInterval];
    //    [newUser removeObjectForKey:data_dp_pad];
    //    [newUser removeObjectForKey:data_pd_pad];
    //    [newUser removeObjectForKey:data_balancelink];
    //    [newUser removeObjectForKey:data_registration_timeout];
    //    [newUser removeObjectForKey:data_header];
    //    [newUser removeObjectForKey:data_footer];
    //    [newUser removeObjectForKey:data_ivr];
    //    [newUser removeObjectForKey:data_proxyPort];
    
    //[newUser setObject:sip_ip forKey:data_sip_ip];
    //    [newUser setObject:proxy_ip forKey:data_proxy_ip];
    //    [newUser setObject:turn_ip forKey:data_turn_ip];
    //    [newUser setObject:enc_key forKey:data_encryption_key];
    //    [newUser setObject:ptime forKey:data_ptime];
    //    [newUser setObject:transport forKey:data_transport];
    //    [newUser setObject:keepalive forKey:data_kaInterval];
    //    [newUser setObject:dp_pad forKey:data_dp_pad];
    //    [newUser setObject:pd_pad forKey:data_pd_pad];
    //    [newUser setObject:balancelink forKey:data_balancelink];
    //    [newUser setObject:@"3000" forKey:data_registration_timeout];
    //    [newUser setObject:header forKey:data_header];
    //    [newUser setObject:footer forKey:data_footer];
    //    [newUser setObject:ivr forKey:data_ivr];
    //    [newUser setObject:proxyPort forKey:data_proxyPort];
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(xmlParserFinishedParsingConfig)]) {
        [self.delegate xmlParserFinishedParsingConfig];
    }
    
}
@end
