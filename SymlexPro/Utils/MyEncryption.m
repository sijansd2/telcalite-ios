//
//  MyEncryption.m
//  SymlexPro
//
//  Created by admin on 7/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "MyEncryption.h"
#import <CommonCrypto/CommonDigest.h>

@implementation MyEncryption

@end


@implementation NSString (MD5)

- (NSString *)MD5String {
    const char *cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
    
    NSString* ss = [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
    ss = [ss lowercaseString];
    return ss;
}

@end
