//
//  XMLParser.h
//  SymlexPro
//
//  Created by USER on 9/14/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol  XMLParserDelegate;

@interface XMLParser : NSObject <NSXMLParserDelegate>
@property (nonatomic,strong) NSMutableString *currentAttributeS;
@property (nonatomic,strong) NSXMLParser *parserS;
@property (weak, nonatomic) id <XMLParserDelegate>delegate;


-(void) parseData: (NSString *)msg;
@end

@protocol XMLParserDelegate<NSObject>
@optional
- (void) xmlParserFinishedParsingConfig;
@end

