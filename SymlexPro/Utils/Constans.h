//
//  Constans.h
//  SymlexPro
//
//  Created by admin on 7/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const baseUrl;

extern NSString * const apiKey;

//API Urls
extern NSString * const voucherRecharge;
extern NSString * const balance;
extern NSString * const balanceTransfer;
extern NSString * const changePass;
extern NSString * const forgetPassword;
extern NSString * const resetPin;
extern NSString * const checkUser;
extern NSString * const checkPin;
extern NSString * const checkMailVerification;
extern NSString * const signUp;
extern NSString * const verifyOtp;
extern NSString * const configURL;
extern NSString * const configTestURL;
extern NSString * const check_user;
extern NSString * const setPin;
//pjsip state
extern NSString * const kSIPCallState;
extern NSString * const kSIPINCOMINGCall;
extern NSString * const kSIPRegState;
extern NSString * const kBalanceState;
extern NSString * const KUpdateCallLog;
extern NSString * const NetworkUpdate;

//registration data
extern NSString * const data_header;
extern NSString * const data_footer;
extern NSString * const data_balancelink;
extern NSString * const data_sip_ip;
extern NSString * const data_proxy_ip;
extern NSString * const data_turn_ip;
extern NSString * const data_encryption_key;
extern NSString * const data_dp_pad;
extern NSString * const data_pd_pad;
extern NSString * const data_ptime;
extern NSString * const data_transport;
extern NSString * const data_kaInterval;
extern NSString * const data_user_name;
extern NSString * const data_password;
extern NSString * const data_caller_id;
extern NSString * const data_opcode;
extern NSString * const data_registration_timeout;
extern NSString * const data_ivr;
extern NSString * const data_apikey;
extern NSString * const data_smsGateway;
extern NSString * const data_country_code;
extern NSString * const data_country_id;
extern NSString * const data_current_balance;
extern NSString * const data_is_linphone_acc;
extern NSString * const data_last_dialed_number;
extern NSString * const data_device_token;
extern NSString * const data_dialed_number;
extern NSString * const data_isAccountCreated;
extern NSString * const data_useTurn;
extern NSString * const data_proxyPort;
extern NSString * const data_showAds;

//registration data
extern NSString * const data_header;
extern NSString * const data_footer;
extern NSString * const data_balancelink;
extern NSString * const data_sip_ip;
extern NSString * const data_proxy_ip;
extern NSString * const data_turn_ip;
extern NSString * const data_encryption_key;
extern NSString * const data_dp_pad;
extern NSString * const data_pd_pad;
extern NSString * const data_ptime;
extern NSString * const data_transport;
extern NSString * const data_kaInterval;
extern NSString * const data_user_name;
extern NSString * const data_password;
extern NSString * const data_caller_id;
extern NSString * const data_opcode;
extern NSString * const data_registration_timeout;
extern NSString * const data_ivr;
extern NSString * const data_apikey;


extern NSString * dialingNumber;
extern NSString * const callTypeDialed;
extern NSString * const callTypeIncoming;
extern NSString * const callTypeFree;

//Release configuration link
extern NSString* const releaseConfigLink;

//push notification server link
extern NSString* const insert_token_link;
extern NSString* const send_push_link;
extern NSString* const decline_push;


@interface Constans : NSObject

@end
