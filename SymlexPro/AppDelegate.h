//
//  AppDelegate.h
//  SymlexPro
//
//  Created by admin on 7/16/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <PushKit/PushKit.h>
#import "GetDataController.h"
#define kBgQueues dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

@interface AppDelegate : UIResponder <UIApplicationDelegate,PKPushRegistryDelegate,GetDataControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *strDeviceToken;


@end

