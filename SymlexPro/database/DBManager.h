//
//  DBManager.h
//  ipjsua
//
//  Created by Riaz Hasan on 12/23/15.
//  Copyright © 2015 Teluu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "LogsHolder.h"



@interface DBManager : NSObject{
    NSString *databasePath;
}

+(DBManager*)getSharedInstance;
-(BOOL)createDB;
-(BOOL) saveData:(LogsHolder*)log;
-(NSArray *) getAllCallLogs;
-(BOOL) deleteRow:(NSString*)callid;
-(BOOL) updateRow:(LogsHolder*)log;
-(BOOL) deletAllRow;
-(BOOL) checkDuplicacy:(LogsHolder*)log;
-(NSArray *) getAllCallLogsByNumber:(NSString *) number;
@end
