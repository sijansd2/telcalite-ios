//
//  LogsHolder.m
//  Pak Tel
//
//  Created by Riaz Hasan on 10/11/15.
//
//

#import "LogsHolder.h"

@implementation LogsHolder


-(instancetype)initWithNumber:(NSString *)number Duration:(NSString*)duration CallID:(NSString *)call_id Date:(NSString*)date Call_status:(NSString*)status{
    
    self = [super init];
    if(self){
        self.number = number;
        self.Duration = duration;
        self.call_id = call_id;
        self.date =date;
        self.call_status=status;
    }
    return self;
}

@end
