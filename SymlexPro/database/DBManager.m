//
//  DBManager.m
//  ipjsua
//
//  Created by Riaz Hasan on 12/23/15.
//  Copyright © 2015 Teluu. All rights reserved.
//

#import "DBManager.h"

static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBManager

+(DBManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"iphonedatabase.db"]];
    
    BOOL isSuccess = YES;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            
            const char *sql_stmt =
            "create table if not exists calllogtable (_id integer primary key, _call_id text, _number text, _date text, _duration text, _call_status text)";
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table calllogtable");
            }
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    
    
    
    
    return isSuccess;
}



- (BOOL) saveData:(LogsHolder*)log
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
        if([self checkDuplicacy:log] ==YES){
            
            NSString *insertSQL = [NSString stringWithFormat:@"insert into calllogtable (_call_id, _number, _date, _duration, _call_status) values (\"%@\",\"%@\", \"%@\", \"%@\",  \"%@\")",log.call_id,log.number, log.date, log.Duration,log.call_status];
            
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_close(database);
                return YES;
            }
            else {
                sqlite3_close(database);
                return NO;
            }
            sqlite3_reset(statement);
        }else{
            
            [self updateRow:log];
            
        }
    }
    return NO;
}


-(BOOL) checkDuplicacy:(LogsHolder*)log{
    
    // NSString *querySQL = [NSString stringWithFormat:@"select _id from calllogtable where _call_id=%@",log.call_id];
    NSString *querySQL = [NSString stringWithFormat:@"select * from calllogtable where _call_id LIKE '%%%@%%'",log.call_id];
    const char *query_stmt = [querySQL UTF8String];
    sqlite3_prepare_v2(database, query_stmt,-1, &statement, NULL);
    
    if (sqlite3_step(statement)==SQLITE_ROW){
        
        NSLog(@"Row Found");
        sqlite3_reset(statement);
        return NO;
    }else{
        sqlite3_reset(statement);
        return YES;
        
    }
    
    
    
}

-(BOOL) deleteRow:(NSString*)callid{
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
        NSString *deleteQuery =[NSString stringWithFormat:@"DELETE FROM calllogtable where _call_id LIKE '%%%@%%'",callid];
        const char *delete_stmt = [deleteQuery UTF8String];
        sqlite3_prepare_v2(database, delete_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_close(database);
            return YES;
        }
        else {
            sqlite3_close(database);
            return NO;
        }
        sqlite3_reset(statement);
        
        
    }
    return NO;
    
    
}

-(BOOL) updateRow:(LogsHolder*)log {
    
    NSLog(@"inside Update Row");
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *updateQuery =
        [NSString stringWithFormat:
         @"UPDATE calllogtable set _number='%@', _date='%@', _duration='%@', _call_status='%@' where _call_id='%@'",log.number,log.date,log.Duration,log.call_status,log.call_id];
        const char *update_stmt = [updateQuery UTF8String];
        sqlite3_prepare_v2(database, update_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Update Successful");
            sqlite3_close(database);
            return YES;
        }
        else {
            NSLog(@"error is %s",sqlite3_errmsg(database));
            sqlite3_close(database);
            return NO;
        }
        
        
    }
    
    return NO;
}


-(BOOL) deletAllRow{
    
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *updateQuery =@"DELETE from calllogtable";
        
        const char *update_stmt = [updateQuery UTF8String];
        sqlite3_prepare_v2(database, update_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Delete all Successful");
            sqlite3_close(database);
            sqlite3_reset(statement);

            return YES;
        }
        else {
            NSLog(@"error is %s",sqlite3_errmsg(database));
            sqlite3_close(database);
            sqlite3_reset(statement);

            return NO;
        }
        
    }
    
    return NO;
}


-(NSArray *) getAllCallLogs{
    
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *resultArray = [[NSMutableArray alloc]init];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {

        NSString *querySQL = @"select * from calllogtable group by _number order by _id DESC";
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW) {
                
                NSString *call_id = [[NSString alloc]
                                     initWithUTF8String:(const char *)
                                     sqlite3_column_text(statement, 1)];
                NSString *number = [[NSString alloc]
                                    initWithUTF8String:(const char *)
                                    sqlite3_column_text(statement, 2)];
                NSString *date = [[NSString alloc]
                                  initWithUTF8String:(const char *)
                                  sqlite3_column_text(statement, 3)];
                NSString *duration = [[NSString alloc]
                                      initWithUTF8String:(const char *)
                                      sqlite3_column_text(statement, 4)];
                NSString *_call_status = [[NSString alloc]
                                          initWithUTF8String:(const char *)
                                          sqlite3_column_text(statement, 5)];
                
                
                
                LogsHolder *temp = [[LogsHolder alloc] initWithNumber:number Duration:duration CallID:call_id Date:date Call_status:_call_status];
                [resultArray addObject:temp];
                
            }
            
            
        }
        
        sqlite3_close(database);
        sqlite3_reset(statement);
    }
    return resultArray;
    
}


-(NSArray *) getAllCallLogsByNumber:(NSString *) number{
    
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *resultArray = [[NSMutableArray alloc]init];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"select * from calllogtable where _number='%@' order by _id DESC",number];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW) {
                
                NSString *call_id = [[NSString alloc]
                                     initWithUTF8String:(const char *)
                                     sqlite3_column_text(statement, 1)];
                NSString *number = [[NSString alloc]
                                    initWithUTF8String:(const char *)
                                    sqlite3_column_text(statement, 2)];
                NSString *date = [[NSString alloc]
                                  initWithUTF8String:(const char *)
                                  sqlite3_column_text(statement, 3)];
                NSString *duration = [[NSString alloc]
                                      initWithUTF8String:(const char *)
                                      sqlite3_column_text(statement, 4)];
                NSString *_call_status = [[NSString alloc]
                                          initWithUTF8String:(const char *)
                                          sqlite3_column_text(statement, 5)];
                
                
                
                LogsHolder *temp = [[LogsHolder alloc] initWithNumber:number Duration:duration CallID:call_id Date:date Call_status:_call_status];
                [resultArray addObject:temp];
                
            }
            
            
        }
        
        sqlite3_close(database);
        sqlite3_reset(statement);
    }
    return resultArray;
    
}


@end
